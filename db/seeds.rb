# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
entity = Entity.create(name: 'Clinique de Sans Taffer')
entity.managers
  .create!(name: 'test', email: 'test@gmail.com', password: 'coucou')

FactoryBot.create_list(:service, 5, entity: entity).each do |service|
  FactoryBot.create_list(:service_referee, rand(10), service: service)
end
