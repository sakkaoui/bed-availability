class AddTokenToServices < ActiveRecord::Migration[6.0]

  def change
    add_column :services, :token, :string
    Service.find_each do |service|
      service.regenerate_token
    end
    change_column :services, :token, :string, null: false
    add_index :services, :token, unique: true
  end

end
