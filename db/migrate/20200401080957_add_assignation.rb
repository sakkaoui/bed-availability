class AddAssignation < ActiveRecord::Migration[6.0]

  def change
    create_table :assignations do |t|
      t.references :service, index: true
      t.references :service_referee, index: true

      t.timestamps
    end
  end

end
