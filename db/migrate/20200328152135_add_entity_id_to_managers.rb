class AddEntityIdToManagers < ActiveRecord::Migration[6.0]

  def change
    add_reference :managers, :entity
    add_index :managers, :email, unique: true
  end

end
