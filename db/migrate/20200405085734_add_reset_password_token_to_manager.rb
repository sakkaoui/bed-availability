class AddResetPasswordTokenToManager < ActiveRecord::Migration[6.0]

  def change
    change_table :managers, bulk: true do |t|
      t.string :password_reset_token
      t.integer :password_reset_valid_until
    end
  end

end
