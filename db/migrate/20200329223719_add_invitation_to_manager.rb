class AddInvitationToManager < ActiveRecord::Migration[6.0]

  def change
    change_table :managers, bulk: true do |t|
      t.string :invitation_token
      t.datetime :invitation_sent_at
      t.datetime :invitation_accepted_at
    end

    add_index :managers, :invitation_token
  end

end
