class AddRelationBetweenServicesAndEntities < ActiveRecord::Migration[6.0]

  def change
    add_reference :services, :entity, index: true
  end

end
