class AddTokenToEntities < ActiveRecord::Migration[6.0]

  def change
    add_column :entities, :token, :string, null: false
    add_index :entities, :token, unique: true
  end

end
