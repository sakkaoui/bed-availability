class AddRelationBetweenServiceRefereesAndServices <
  ActiveRecord::Migration[6.0]

  def change
    add_reference :service_referees, :service, index: true
  end

end
