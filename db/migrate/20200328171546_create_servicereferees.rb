class CreateServicereferees < ActiveRecord::Migration[6.0]

  def change
    create_table :service_referees do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :password_digest, null: false
      t.text :contact_details

      t.timestamps
    end
  end

end
