class AddMissingFieldsToServices < ActiveRecord::Migration[6.0]

  def change
    change_table :services, bulk: true do |t|
      t.integer :current_availability, default: 0, null: false
      t.integer :planned_arrivals_count, default: 0, null: false
      t.integer :planned_leaves_count, default: 0, null: false
    end

  end

end
