class AddContactToServices < ActiveRecord::Migration[6.0]

  def change
    add_column :services, :contact, :text
  end

end
