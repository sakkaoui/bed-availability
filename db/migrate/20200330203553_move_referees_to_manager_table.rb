class MoveRefereesToManagerTable < ActiveRecord::Migration[6.0]

  def up
    add_column :managers, :type, :string, null: false, default: "Manager"
    add_column :managers, :contact_details, :text
    add_reference :managers, :service, index: true

    ServiceReferee.find_each do |referee|
      Manager.create(
        name: referee.name,
        email: referee.email,
        password_digest: referee.password_digest,
        contact_details: referee.contact_details,
        entity_id: referee.service.entity_id,
        service_id: referee.service_id,
        type: "ServiceReferee"
      )
    end

    drop_table :service_referees
  end

  def down
    raise ActiveRecord::IrreversibleMigration, "Can't redo ServiceReferees..."
  end

end
