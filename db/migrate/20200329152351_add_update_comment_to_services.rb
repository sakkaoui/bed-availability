class AddUpdateCommentToServices < ActiveRecord::Migration[6.0]

  def change
    add_column :services, :update_comment, :text
  end

end
