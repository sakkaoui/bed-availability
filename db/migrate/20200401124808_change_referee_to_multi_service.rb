class ChangeRefereeToMultiService < ActiveRecord::Migration[6.0]

  def up
    ServiceReferee.where.not(id: nil).find_each do |referee|
      Assignation.create(
        service_referee: referee,
        service: Service.where(id: referee.service_id).first
      )
    end

    remove_column :managers, :service_id
  end

  def down
    add_reference :managers, :service

    ServiceReferee.find_each do |referee|
      referee.update(service_id: referee.services.first.id)
      referee.assignations.destroy_all
    end
  end

end
