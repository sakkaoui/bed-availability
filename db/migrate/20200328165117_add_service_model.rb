class AddServiceModel < ActiveRecord::Migration[6.0]

  def change
    create_table :services do |t|
      t.string :name, null: false
      t.integer :floor
      t.integer :total_bed_count, default: 0
      t.integer :available_women_bed_count, default: 0
      t.integer :available_man_bed_count, default: 0
      t.integer :available_gender_free_bed_count, default: 0
      t.integer :available_isolated_bed_count, default: 0
      t.text :comment

      t.timestamps
    end
  end

end
