class AddAcceptNotificationToAssignations < ActiveRecord::Migration[6.0]

  def change
    add_column :assignations, :accept_notifications, :boolean, default: true
  end

end
