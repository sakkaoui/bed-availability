class CreateNotificationSubscriptions < ActiveRecord::Migration[6.0]

  def change
    create_table :notification_subscriptions do |t|
      t.string :email, null: false
      t.references :service, null: false, foreign_key: true, index: true

      t.timestamps
    end
  end

end
