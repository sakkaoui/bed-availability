# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_17_150704) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignations", force: :cascade do |t|
    t.bigint "service_id"
    t.bigint "service_referee_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "accept_notifications", default: true
    t.index ["service_id"], name: "index_assignations_on_service_id"
    t.index ["service_referee_id"], name: "index_assignations_on_service_referee_id"
  end

  create_table "entities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "token", null: false
    t.index ["token"], name: "index_entities_on_token", unique: true
  end

  create_table "managers", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "entity_id"
    t.string "invitation_token"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.string "type", default: "Manager", null: false
    t.text "contact_details"
    t.string "password_reset_token"
    t.integer "password_reset_valid_until"
    t.index ["email"], name: "index_managers_on_email", unique: true
    t.index ["entity_id"], name: "index_managers_on_entity_id"
    t.index ["invitation_token"], name: "index_managers_on_invitation_token"
  end

  create_table "notification_subscriptions", force: :cascade do |t|
    t.string "email", null: false
    t.bigint "service_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["service_id"], name: "index_notification_subscriptions_on_service_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "name", null: false
    t.integer "floor"
    t.integer "total_bed_count", default: 0
    t.integer "available_women_bed_count", default: 0
    t.integer "available_man_bed_count", default: 0
    t.integer "available_gender_free_bed_count", default: 0
    t.integer "available_isolated_bed_count", default: 0
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "entity_id"
    t.text "update_comment"
    t.string "token", null: false
    t.string "uds"
    t.text "contact"
    t.integer "current_availability", default: 0, null: false
    t.integer "planned_arrivals_count", default: 0, null: false
    t.integer "planned_leaves_count", default: 0, null: false
    t.index ["entity_id"], name: "index_services_on_entity_id"
    t.index ["token"], name: "index_services_on_token", unique: true
  end

  add_foreign_key "notification_subscriptions", "services"
end
