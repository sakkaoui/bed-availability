class Service < ApplicationRecord

  has_secure_token :token

  %i[
    total_bed_count
    available_women_bed_count
    available_man_bed_count
    available_gender_free_bed_count
    available_isolated_bed_count
  ].each do |attribute|
    validates attribute,
              presence: true,
              numericality: { greater_than_or_equal_to: 0 }
  end

  validates :name, presence: true
  validates :token, uniqueness: true
  validate :total_beds_should_be_less_than_availables
  validate :verify_planned_vs_available, on: :update

  has_many :notification_subscriptions,
           dependent: :destroy,
           inverse_of: :service
  has_many :assignations, dependent: :destroy
  has_many :service_referees,
           dependent: :destroy,
           through: :assignations,
           inverse_of: :services do
             def notifiable
               where("assignations.accept_notifications = ?", true)
             end
           end
  belongs_to :entity, inverse_of: :services

  def sum_available_beds
    countable_attibutes = %w[
      available_women_bed_count
      available_man_bed_count
      available_gender_free_bed_count
      available_isolated_bed_count
    ]

    attributes.slice(*countable_attibutes).values.compact.sum
  end

  def self.generate_unique_secure_token
    SecureRandom.base58(45)
  end

  def notify(count = 0)
    elapsed_time = (Time.zone.now - self.updated_at) / 60
    return if elapsed_time < 30 + 30 * count.to_i

    emails = []
    emails += self.service_referees.notifiable.map(&:email)
    notif_create_column = NotificationSubscription.arel_table[:created_at]
    emails += self.notification_subscriptions
      .where(notif_create_column.gt(24.hours.ago))
      .map(&:email)

    return if emails.empty?

    NotificationMailer.with(
      service: self,
      emails: emails.uniq
    ).notify.deliver
  end

  private

  def total_beds_should_be_less_than_availables
    return if sum_available_beds <= (total_bed_count || 0)

    errors.add(:base, I18n.t('activerecord.errors.service.invalid_total'))
  end

  def verify_planned_vs_available
    return unless current_availability.present? &&
                  planned_arrivals_count.present? &&
                  planned_leaves_count.present?

    if sum_available_beds <=
       (current_availability.to_i +
        planned_leaves_count.to_i -
        planned_arrivals_count.to_i)
      return
    end

    errors.add(:base, I18n.t('activerecord.errors.service.missmatch_total'))
  end

end
