module AuthenticableModel
  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i.freeze
  TTL = 3.months

  def self.included(base)
    base.send :include, InstanceMethods
    base.extend ClassMethods
    base.add_callback
    base.add_validations
    base.add_secure_attributes
  end

  module ClassMethods
    def add_callback
      before_validation :lower_case_email
    end

    def add_validations
      validates :email, presence: true, uniqueness: true, format: EMAIL_REGEXP
      validates :password_digest, presence: true
      validates :password_reset_token, uniqueness: true, allow_blank: true
    end

    def add_secure_attributes
      has_secure_password
      has_secure_token :password_reset_token
    end

    def from_auth_token(token)
      payload = JWT.decode(
        token,
        ENV.fetch('RAILS_MASTER_KEY') { '' },
        true,
        { algorithm: 'HS256' }
      ).first
      Manager.find(payload['id'].to_i)
    end
  end

  module InstanceMethods
    def lower_case_email
      self.email = self.email&.downcase
    end

    def auth_token
      payload = { id: self.id, exp: TTL.from_now.to_i }
      hmac_secret = ENV.fetch('RAILS_MASTER_KEY') { '' }
      JWT.encode payload, hmac_secret, 'HS256'
    end

    def generate_password_reset_token
      regenerate_password_reset_token
      update_column(:password_reset_valid_until, 1.hour.from_now.to_i)
      password_reset_token
    end

    def password_reset_token_expired?
      password_reset_valid_until.to_i < Time.zone.now.to_i
    end
  end
end
