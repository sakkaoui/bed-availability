class ServiceReferee < Manager

  has_many :assignations, dependent: :destroy
  has_many :services, through: :assignations, inverse_of: :service_referees

end
