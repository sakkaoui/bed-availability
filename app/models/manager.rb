class Manager < ApplicationRecord

  include AuthenticableModel

  has_secure_token :invitation_token

  validates :name, presence: true
  validates :invitation_token, uniqueness: true, allow_nil: true

  belongs_to :entity, inverse_of: :managers

  def accept_invitation
    update(
      invitation_token: nil,
      invitation_accepted_at: Time.zone.now,
    )
  end

end
