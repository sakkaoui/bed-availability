class Entity < ApplicationRecord

  has_secure_token :token

  validates :name, presence: true
  validates :token, uniqueness: true

  has_many :managers, dependent: :destroy, inverse_of: :entity
  has_many :services, dependent: :destroy, inverse_of: :entity

  accepts_nested_attributes_for :managers

  def self.generate_unique_secure_token
    SecureRandom.base58(45)
  end

end
