class Assignation < ApplicationRecord

  belongs_to :service, optional: false
  belongs_to :service_referee, optional: false

  validates :service, presence: true
  validates :service_referee, presence: true
  validates :accept_notifications, inclusion: { in: [true, false] }

end
