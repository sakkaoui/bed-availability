class NotificationSubscription < ApplicationRecord

  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i.freeze

  validates :email, presence: true, format: EMAIL_REGEXP

  belongs_to :service, inverse_of: :notification_subscriptions

  before_validation :lower_case_email

  private

  def lower_case_email
    self.email = self.email&.downcase
  end

end
