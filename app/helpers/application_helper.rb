module ApplicationHelper
  def elapsed_class(service)
    update_delay = (Time.zone.now - service.updated_at) / 60
    return 'recent' if update_delay < 30
    return 'old' if update_delay > 180
    return ''
  end

  def color_class(current, total)
    pourcent = (current + 0.0) / total * 100
    return 'red' if pourcent < 5
    return 'orange' if pourcent < 15
    return 'green'
  end
end
