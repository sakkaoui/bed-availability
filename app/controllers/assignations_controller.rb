class AssignationsController < ApplicationController

  before_action :require_login

  def disable_notifications
    toogle_notifications(false)
  end

  def enable_notifications
    toogle_notifications(true)
  end

  private

  def toogle_notifications(direction)
    @service = Service.find(params[:service_id])
    assignation = @service.assignations
      .find_by(service_referee: current_user)

    if assignation && assignation.update(accept_notifications: direction)
      flash[:success] = t("assignations.flash.notifications_#{direction}.success")
    else
      flash[:error] = t("assignations.flash.notifications_#{direction}.errors")
    end

    redirect_to edit_availabilities_path(token: @service.token)
  rescue ActiveRecord::RecordNotFound => e
    redirect_to root_url
  end

end