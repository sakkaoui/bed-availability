class ServiceRefereesController < ApplicationController

  before_action :require_login

  def new
    @service = current_entity.services.where(id: params[:service_id]).first
    unless @service
      redirect_to root_url
      return
    end
    @referee = ServiceReferee.new(
      entity: current_entity,
      services: [@service]
    )
  end

  def create
    @service = current_entity.services.where(id: params[:service_id]).first
    unless @service
      redirect_to root_url
      return
    end

    @referee = current_entity.managers.where(
      email: create_manager_params[:email]
    ).first
    is_referee_creation = false
    unless @referee
      @referee = ServiceReferee.new(
        create_manager_params.merge(entity: current_entity)
      )
      is_referee_creation = true
    end

    if @service.service_referees.where(email: @referee.email).exists?
      flash[:error] = t('service_referees.flash.create.already_exists')
    else
      @referee.services << @service
      if @referee.save
        if is_referee_creation
          InvitationMailer.with(
            manager: @referee
          ).create_referee.deliver_later
        else
          InvitationMailer.with(
            service_referee: @referee,
            service: @service
          ).add_referee.deliver_later
        end
        flash[:success] = t('service_referees.flash.create.success')
      else
        flash[:error] = t('service_referees.flash.create.error')
        render :new
        return
      end
    end

    redirect_to(services_url)
  end

  def destroy
    @assignation = Assignation.where(
      service_id: params[:service_id],
      service_referee_id: params[:id]
    ).first
    if @assignation&.destroy
      if Assignation.where(service_referee_id: params[:id]).count == 0
        ServiceReferee.where(id: params[:id]).first.destroy
      end
      flash[:notice] = t('service_referees.flash.destroy.success')
    else
      flash[:notice] = t('service_referees.flash.destroy.error')
    end
    redirect_to new_service_service_referee_path
  end

  private

  def create_manager_params
    params
      .require(:service_referee)
      .permit(:name, :email, :contact_details)
      .merge(password_digest: "passw0rd")
  end

end
