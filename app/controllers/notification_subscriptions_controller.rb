class NotificationSubscriptionsController < ApplicationController

  def create
    @service = Service.where(token: params[:token]).first
    unless @service
      redirect_to root_path
      return
    end

    if @service.notification_subscriptions
        .where(email: permitted_params[:email]).exists?
      flash[:error] = t('notification_subscriptions.flash.create.already_exist')
    else
      notif = @service.notification_subscriptions.build(permitted_params)
      if notif.save
        flash[:success] = t('notification_subscriptions.flash.create.success')
        set_notification_subscription_id(@service, notif.id)
      else
        flash[:error] =
          t(
            'notification_subscriptions.flash.create.errors',
            errors: notif.errors.full_messages.first
          )
      end
    end

    redirect_to edit_availabilities_path(token: @service.token)
  end

  def destroy
    @service = Service.where(token: params[:token]).first
    unless @service
      redirect_to root_path
      return
    end

    @notification = @service.notification_subscriptions
      .where(id: params[:id]).first

    if @notification
      @notification.destroy
      unset_notification_subscription_id(@service)
      flash[:notice] = t('notification_subscriptions.flash.destroy.success')
    else
      flash[:notice] = t('notification_subscriptions.flash.destroy.errors')
    end

    redirect_to edit_availabilities_path(token: @service.token)
  end

  private

  def permitted_params
    params.require(:notification_subscription).permit(:email)
  end

end
