module Authenticable
  extend ActiveSupport::Concern

  class_methods do
    def use_login_class(value = "Manager")
      @login_class = value
    end

    def login_class
      @login_class || "Manager"
    end
  end

  included do
    use_login_class
    helper_method :current_user
  end

  def login_variable_name
    "@current_#{self.class.login_class.downcase}"
  end

  def token
    params[:auth_token] || token_from_request_headers || cookies.signed[:jwt]
  end

  def token_from_request_headers
    request.headers['Authorization']&.split&.last
  end

  def current_user
    if instance_variable_defined?(login_variable_name)
      return instance_variable_get(login_variable_name)
    end

    begin
      instance_variable_set(
        login_variable_name,
        token ? self.class.login_class.constantize.from_auth_token(token) : nil
      )
    rescue StandardError
      instance_variable_set(login_variable_name, nil)
    end
  end

  def require_login
    instance_variable_set(
      login_variable_name,
      self.class.login_class.constantize.from_auth_token(token)
    )
  rescue JWT::DecodeError
    redirect_to root_url,
                flash: { error: t('sessions.flash.must_be_connected') }
  rescue JWT::ExpiredSignature
    redirect_to root_url,
                flash: { error: t('sessions.flash.must_be_connected') }
  end

  def set_authentication_cookie(entity)
    cookies.signed[:jwt] = {
      value: entity.auth_token,
      expires: entity.class::TTL.from_now,
      httponly: true
    }
  end

  def authenticate(entity, password)
    return false unless entity

    if entity.authenticate(password)
      set_authentication_cookie(entity)
      return true
    end

    false
  end
end
