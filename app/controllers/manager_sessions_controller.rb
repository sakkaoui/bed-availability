class ManagerSessionsController < ApplicationController

  before_action :require_login, only: :destroy

  def new
  end

  def create
    @manager = Manager.where(email: auth_params[:email]).first

    if authenticate(@manager, auth_params[:password])
      redirect_to root_url, notice: t('manager_sessions.flash.sign_in.success')
    else
      flash.now[:alert] = t('manager_sessions.flash.sign_in.error')
      render :new
    end
  end

  def destroy
    cookies.delete(:jwt)
    redirect_to root_url, notice: t('manager_sessions.flash.sign_out.success')
  end

  private

  def auth_params
    params.permit(:email, :password)
  end

end
