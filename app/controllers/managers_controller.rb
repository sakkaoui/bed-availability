class ManagersController < ApplicationController

  before_action :require_login

  def new
    @manager = current_entity.managers.build
  end

  def create
    @manager = current_entity.managers.build(create_manager_params)

    unless @manager.save
      flash[:error] = t('managers.flash.create.error')
      render :new
      return
    end

    InvitationMailer.with(
      manager: @manager
    ).create_referee.deliver_later
    redirect_to(
      root_url,
      flash: { success: t('managers.flash.create.success') },
    )
  end

  def destroy
    if current_entity.managers.where(id: params[:id]).first&.destroy
      flash[:notice] = t('managers.flash.destroy.success')
    else
      flash[:notice] = t('managers.flash.destroy.error')
    end
    redirect_to new_manager_path
  end

  private

  def create_manager_params
    params
      .require(:manager)
      .permit(:name, :email)
      .merge(password_digest: "passw0rd")
  end

end
