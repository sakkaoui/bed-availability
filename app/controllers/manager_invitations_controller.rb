class ManagerInvitationsController < ApplicationController

  def new
    @manager = Manager.find_by(invitation_token: params[:token])
    render :new && return if @manager

    redirect_to root_url, flash: {
      error: t('manager_invitations.flash.update.bad_token')
    }
  end

  def update
    @manager = Manager.find_by(invitation_token: params[:token])

    unless @manager
      redirect_to(
        root_url,
        flash: { error: t('manager_invitations.flash.update.bad_token') },
      ) && return
    end

    @manager.password_digest = nil
    if @manager.update(invitation_params)
      @manager.accept_invitation
      set_authentication_cookie(@manager)
      redirect_to(
        root_url,
        flash: { success: t('manager_invitations.flash.update.success') },
      ) && return
    end

    flash[:error] = t('manager_invitations.flash.update.error')
    render :new
  end

  private

  def invitation_params
    hash = params.permit(
      manager: %i[name password password_confirmation],
      service_referee: %i[name password password_confirmation]
    )
    hash = hash[:manager] || hash[:service_referee]
    hash[:password_confirmation] = "" if hash[:password_confirmation].nil?

    hash
  end

end
