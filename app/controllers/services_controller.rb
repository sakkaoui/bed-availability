class ServicesController < ApplicationController

  before_action :require_login,
                except: %i[
                  edit_availabilities
                  update_availabilities
                  qrcode
                  show
                ]

  def index
    @services = current_entity&.services
    redirect_to root_url && return unless @services

    @services = @services
      .includes(:service_referees)
      .order(floor: :asc, name: :asc)
  end

  def new
    @service = current_entity&.services&.build
    redirect_to root_url unless @service
  end

  def create
    @service = current_entity&.services&.build(permitted_params_for_create)
    redirect_to root_url && return unless @service

    if @service.save
      redirect_to services_url,
                  flash: { success: t('services.flash.create.success') }
      return
    end

    flash[:error] = t('services.flash.create.error')
    render :new
  end

  def edit
    @service = current_entity&.services&.find_by(id: params[:id])
    redirect_to root_url unless @service
  end

  def update
    @service = current_entity&.services&.find_by(id: params[:id])
    redirect_to root_url && return unless @service

    if @service.update(permitted_params_for_create)
      redirect_to services_url,
                  flash: { success: t('services.flash.update.success') }
      return
    end

    flash[:error] = t('services.flash.update.error')
    render :new
  end

  def destroy
    service = current_entity&.services&.find_by(id: params[:id])
    redirect_to root_url && return unless service

    if service.destroy
      flash[:success] = t('services.flash.destroy.success')
    else
      flash[:error] = t('services.flash.destroy.error')
    end

    redirect_to services_url
  end

  def edit_availabilities
    @service = Service.where(token: params[:token]).first

    if @service
      @notification_subscription =
        NotificationSubscription
          .where(id: get_notification_subscription_id(@service)).first ||
        @service.notification_subscriptions.build
    else
      redirect_to root_path
    end
  end

  def update_availabilities
    @service = Service.where(token: params[:token]).first
    unless @service
      redirect_to root_path
      return
    end

    if @service.update(permitted_params_for_update) && @service.touch
      flash[:success] = t('services.flash.availabilities.success')
      redirect_to edit_availabilities_path(token: @service.token)
    else
      @step_2 = true
      flash[:error] = t('services.flash.availabilities.error')
      render :edit_availabilities
    end
  end

  def qrcode
    @service = Service.where(token: params[:token]).first
    redirect_to root_url unless @service
  end

  def show
    @entity = Entity.where(token: params[:token]).first
    @service = @entity.services.where(id: params[:id]).first if @entity
    redirect_to root_url unless @service
  end

  private

  def permitted_params_for_update
    params.require(:service).permit(
      :current_availability,
      :planned_leaves_count,
      :planned_arrivals_count,
      :available_women_bed_count,
      :available_man_bed_count,
      :available_gender_free_bed_count,
      :available_isolated_bed_count,
      :update_comment
    )
  end

  def permitted_params_for_create
    params
      .require(:service)
      .permit(:name, :floor, :total_bed_count, :comment, :uds)
  end

end
