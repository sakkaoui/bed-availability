class EntitiesController < ApplicationController

  def availabilities
    @entity = Entity.where(token: params[:token]).first
    if @entity
      @services = @entity.services.order(floor: :asc, name: :asc)
    else
      redirect_to root_url
    end
  end

  def qrcode
    @entity = Entity.where(token: params[:token]).first
    redirect_to root_url unless @entity
  end

  def new
    @entity = Entity.new
    @entity.managers.build
  end

  def create
    @entity = Entity.new(entity_params)
    if @entity.save
      set_authentication_cookie(@entity.managers.first)
      redirect_to root_url,
                  flash: { success: t('entities.flash.create.success') }
      return
    end

    flash[:error] = t('entities.flash.create.error')
    render :new
  end

  private

  def entity_params
    hash = params.require(:entity).permit(
      :name,
      managers_attributes: %i[name email password password_confirmation],
    )

    if hash.dig(:managers_attributes, '0', :password_confirmation).nil?
      hash[:managers_attributes]['0'][:password_confirmation] = ""
    end
    hash
  end

end
