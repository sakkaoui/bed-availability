class PasswordsController < ApplicationController

  def new
  end

  def create
    manager = Manager.find_by(email: params[:email])
    unless manager
      flash[:error] = t('passwords.flash.create.bad_email')
      render :new
      return
    end

    manager.generate_password_reset_token
    PasswordMailer.with(manager_id: manager.id).reset.deliver_later
    redirect_to(
      sign_in_managers_url,
      flash: {
        success: t('passwords.flash.create.success', email: manager.email),
      }
    )
  end

  def edit
    @manager = Manager.find_by(password_reset_token: params[:token])
    return if @manager

    redirect_to sign_in_managers_url,
                flash: { error: t('passwords.flash.update.bad_token') }
  end

  def update
    @manager = Manager.find_by(password_reset_token: params[:token])

    if @manager.nil? || @manager.password_reset_token_expired?
      flash[:error] = t('passwords.flash.update.bad_token')
      render :edit
      return
    end

    if @manager.update(password_params)
      set_authentication_cookie(@manager)
      redirect_to root_url,
                  flash: { success: t('passwords.flash.update.success') }
      return
    end

    flash[:error] = t('passwords.flash.update.error')
    render :edit
  end

  private

  def password_params
    hash = params.permit(:password, :password_confirmation)
    hash[:password_confirmation] = "" if hash[:password_confirmation].nil?
    hash
  end

end
