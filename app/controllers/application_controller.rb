class ApplicationController < ActionController::Base

  include Authenticable

  def current_entity
    @current_entity ||= @current_manager&.entity
  end
  helper_method :current_entity

  def get_notification_subscription_id(service)
    table = JSON.parse(cookies.signed[:notification_subscription_id] || "{}")
    table[service.id.to_s]
  end

  def set_notification_subscription_id(service, id)
    table = JSON.parse(cookies.signed[:notification_subscription_id] || "{}")
    table[service.id] = id
    cookies.signed[:notification_subscription_id] = {
      value: table.to_json,
      expires: 24.hours.from_now,
      httponly: true
    }
  end

  def unset_notification_subscription_id(service)
    table = JSON.parse(cookies.signed[:notification_subscription_id] || "{}")
    table.delete(service.id.to_s)
    cookies.signed[:notification_subscription_id] = {
      value: table.to_json,
      expires: 24.hours.from_now,
      httponly: true
    }
  end

end
