class InvitationMailer < ApplicationMailer

  def create_referee
    @manager = params[:manager]
    mail({
      to: @manager.email,
      subject: I18n.t(
        "mailer.invitation.create_referee.subject", name: @manager.entity.name
      ),
    })
    @manager.update(invitation_sent_at: Time.zone.now)
  end

  def add_referee
    @referee = params[:service_referee]
    @service = params[:service]
    mail({
      to: @referee.email,
      subject: I18n.t(
        "mailer.invitation.add_referee.subject",
        service_name: @service.name
      ),
    })
  end

end
