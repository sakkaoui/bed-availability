class PasswordMailer < ApplicationMailer

  def reset
    @manager = Manager.find(params[:manager_id])
    mail({
      to: @manager.email,
      subject: I18n.t("mailer.password.reset.subject")
    })
  end

end
