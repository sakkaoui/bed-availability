class ApplicationMailer < ActionMailer::Base

  default from: 'noreply@lidispo.fr'
  layout 'mailer'

end
