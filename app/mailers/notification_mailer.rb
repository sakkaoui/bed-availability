class NotificationMailer < ApplicationMailer

  def notify
    @service = params[:service]
    mail({
      bcc: params[:emails],
      subject: I18n.t("mailer.notification.notify.subject")
    })
  end

end
