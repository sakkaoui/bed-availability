document.addEventListener('turbolinks:load', function() {
  var button = document.getElementById("notifications-permission-request");
  if (!button) {
    return;
  }
  button.parentNode.parentNode.style.display = 'none';

  if (!window.Notification) {
    return;
  }

  var permission = Notification.permission;
  if (permission !== 'granted') {
    button.parentNode.parentNode.style.display = 'block';
    button.addEventListener('click', function(e) {
      Notification.requestPermission().then(function(result) {
        if(result === 'granted') {
          console.log("[Notification] permission is granted");
        }
        Location.reload;
      });
    });
  }
})