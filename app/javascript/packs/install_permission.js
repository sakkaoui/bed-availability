document.addEventListener('turbolinks:load', function() {
  let deferredPrompt;
  const addBtn = document.querySelector('#installation-button');
  if (!addBtn) {
    return;
  }
  addBtn.parentNode.parentNode.style.display = 'none';

  window.addEventListener('beforeinstallprompt', (e) => {
    e.preventDefault();
    deferredPrompt = e;

    addBtn.parentNode.parentNode.style.display = 'block';
    console.log(addBtn.parentNode.parentNode.style)

    addBtn.addEventListener('click', (e) => {
      addBtn.parentNode.parentNode.style.display = 'none';
      deferredPrompt.prompt();

      deferredPrompt.userChoice.then((choiceResult) => {
          if (choiceResult.outcome === 'accepted') {
            console.log('User accepted the A2HS prompt');
          } else {
            console.log('User dismissed the A2HS prompt');
            addBtn.parentNode.parentNode.style.display = "block"
          }
          deferredPrompt = null;
        });
    });
  });
})
