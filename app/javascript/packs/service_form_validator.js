function checkFormValidity(form, next_button, verification_errors) {
  let leaves = parseInt(form[3].value);
  let arrivals = parseInt(form[4].value);
  let current = parseInt(form[2].value);
  let sum = (current || 0) + (leaves || 0) - (arrivals || 0)

  if ( sum >= 0) {
    verification_errors.style.display = "none";
    next_button.disabled = false
    document.getElementById("available-count-confirmation").textContent = 
      sum;
  } else {
    next_button.disabled = "disabled"
    verification_errors.style.display = "block";
  }
  
  if (!isNaN(current) && !isNaN(arrivals) && !isNaN(leaves) && sum >= 0) {
    next_button.disabled = false
  } else {
    next_button.disabled = "disabled"
  }
}

document.addEventListener('turbolinks:load', function() {
  let service_update_form =
    document.getElementById("service-update-form");
  if (service_update_form !== null) {
    let next_button = document.getElementById('next_button');
    let previous_button = document.getElementById('previous_button');
    let verification_errors = document.getElementById(
      "service_update_step1_errors"
    );

    checkFormValidity(service_update_form, next_button, verification_errors)

    service_update_form.addEventListener('change', function(event) {
      checkFormValidity(event.target.form, next_button, verification_errors)
    })

    if (next_button !== null) {
      next_button.addEventListener('click', function(event) {
        event.stopPropagation()
        event.preventDefault()
        document.getElementById('service_update_step1').style.display = "none";
        document.getElementById('service_update_step2').style.display = "block";
      })
    }
    if (previous_button !== null) {
      previous_button.addEventListener('click', function(event) {
        event.stopPropagation()
        event.preventDefault()
        document.getElementById('service_update_step1').style.display = "block";
        document.getElementById('service_update_step2').style.display = "none";
      })
    }
  }
});