const jsQR = require("jsqr");

let scanIntervalId = null;

function takePicture(videoElement) {
  const canvas = document.createElement("canvas");
  canvas.width = videoElement.clientWidth;
  canvas.height = videoElement.clientHeight;
  const canvasContext = canvas.getContext('2d');

  canvasContext.drawImage(
    videoElement, 0, 0, canvas.width, canvas.height
  );

  return canvasContext.getImageData(0, 0, canvas.width, canvas.height);
}

function scan() {
  let videoElement = document.getElementById('qrcode-scanner');
  let imageData = takePicture(videoElement);

  const code = jsQR(
    imageData.data,
    videoElement.clientWidth,
    videoElement.clientHeight
  );

  if (code && code.data) {
    let messageBox = document.getElementById('loadingMessage')
    messageBox.innerHTML = "✌️ Un QR Code a été trouvé !"
    clearInterval(scanIntervalId)
    window.location = code.data;
  }
}

function startScanning() {
  scanIntervalId = setInterval(scan, 2000);
}

document.addEventListener('turbolinks:load', function() {
  let scannerContainer = document.getElementById('qrcode-scanner');
  if (!scannerContainer)
    return;

  scannerContainer.setAttribute('autoplay', '');
  scannerContainer.setAttribute('muted', '');
  scannerContainer.setAttribute('playsinline', '');

  let messageBox = document.getElementById('loadingMessage')
  messageBox.innerHTML = "🎥 En attente d'un flux vidéo..."

  navigator.mediaDevices.getUserMedia({
    audio: false,
    video: { facingMode: "environment" }
  }).then(function(stream) {
    messageBox.innerHTML = "🔎 Recherche d'un QR Code dans l'image..."
    scannerContainer.srcObject = stream;
    scannerContainer.onloadedmetadata = function(e) {
      scannerContainer.play();
      startScanning();
    };
  }).catch(function(err) {
    messageBox.innerHTML = "❗️ Il y a une erreur avec le flux video..."
    console.log(err.name + ": " + err.message);
  });
})