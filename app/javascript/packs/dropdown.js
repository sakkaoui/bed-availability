document.addEventListener('turbolinks:load', function() {
  let dropdowns = document.querySelectorAll(".dropdown");

  dropdowns.forEach(function(dropdown) {
    let content = dropdown.querySelector('.dropdown_content')
    let link = dropdown.querySelector('.dropdown_head')
    content.style.display = "none"

    dropdown.addEventListener('click', function(event) {
      if (event.target.classList.contains("dropdown_head")) {
        event.preventDefault();
        event.stopPropagation();
        let old_display = content.style.display
        dropdowns.forEach(function(d) {
          d.querySelector('.dropdown_content').style.display = "none"
          d.querySelector('.dropdown_head').classList.remove("open")
        })
        if (old_display === "none") {
          content.style.display = "block"
          link.classList.add("open")
        }
      }
    })
  })
})