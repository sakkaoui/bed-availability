import QRCode from "./qrcode"

document.addEventListener('turbolinks:load', function() {
  var qrContainer = document.getElementById("qrcode");
  if (!qrContainer) {
    return;
  }

  var qrcode = new QRCode(qrContainer, {
      width : 200,
      height : 200,
      useSVG: true
  });
  qrcode.makeCode(qrContainer.dataset.url);
})