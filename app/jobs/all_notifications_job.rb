class AllNotificationsJob < ApplicationJob

  def perform(options)
    return if options[:count].to_i >= 3

    Service.find_each do |service|
      ServiceNotificationJob
        .set(queue: :notifications)
        .perform_later(service, options[:count].to_i)
    end

    AllNotificationsJob
      .set(wait: 30.minutes, queue: :notifications)
      .perform_later(count: options[:count].to_i + 1)
  end

end
