class ServiceNotificationJob < ApplicationJob

  def perform(service, count)
    service.notify(count.to_i)
  end

end
