source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.2', '>= 6.0.2.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 4.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
gem 'bcrypt', '~> 3.1.7'
# Use JWT to generate API token
gem 'jwt'

# asynchronous jobs
gem 'sidekiq'
gem "sidekiq-cron", "~> 1.1"

# translations for rails
gem 'rails-i18n', '~> 6.0.0'

# Ease the inclusion of service worker and manifest files in asset pipeline
gem 'serviceworker-rails'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# Exception notifier
gem 'exception_notification'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  gem "factory_bot_rails"
  gem "faker"
  gem 'rspec-rails', '~> 4.0.0.beta2'
  gem 'rails-controller-testing'
  gem "simplecov", require: false
  gem "shoulda-matchers"
  gem "timecop"

  # dot env to use .env file for configuration
  gem 'dotenv-rails', require: 'dotenv/rails-now'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # check code style with rubocop
  gem 'rubocop', require: false
  # add rails linter
  gem 'rubocop-rails', require: false
  # add rspec linter
  gem 'rubocop-rspec', require: false
  # add performance linter
  gem 'rubocop-performance', require: false

  # pronto to extract diff and format result of linter
  gem 'pronto'
  # pronto binding for rubocop
  gem 'pronto-rubocop', require: false
  # pronto binding for flay
  gem 'pronto-flay', require: false
  # pronto binding for bundler audit
  gem 'pronto-bundler_audit', require: false

end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
