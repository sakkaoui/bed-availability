Rails.application.routes.default_url_options[:host] = ENV['DEFAULT_URL_HOST']
Rails.application.routes.default_url_options[:protocol] =
  Rails.env.development? ? "http" : "https"
