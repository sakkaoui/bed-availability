Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :entities, only: %i[new create]

  resources :managers, only: %i[new create destroy] do
    collection do
      # manager sessions
      get 'sessions/new', to: 'manager_sessions#new', as: :sign_in
      post 'sessions', to: 'manager_sessions#create'
      delete 'session', to: 'manager_sessions#destroy', as: :sign_out

      # manager invitations
      get 'invitations/:token',
          to: 'manager_invitations#new',
          as: :new_invitation
      patch 'invitations', to: 'manager_invitations#update'
    end
  end

  get 'passwords/lost', to: 'passwords#new', as: :new_password
  post 'passwords', to: 'passwords#create'
  get 'passwords/renew/:token', to: 'passwords#edit', as: :renew_password
  patch 'passwords', to: 'passwords#update'

  resources :services do
    resources :service_referees, only: %i[new create destroy]
    resources :assignations, only: [] do
      collection do
        delete :disable_notifications
        post :enable_notifications
      end
    end
  end
  get "/services/qrcode/:token",
      to: "services#qrcode",
      as: :service_qrcode
  get 'services/:id/:token', to: 'services#show'

  get "/entities/availabilities/:token",
      to: "entities#availabilities",
      as: :entity_availabilities
  get "/entities/qrcode/:token",
      to: "entities#qrcode",
      as: :entity_qrcode
  get "/services/availabilities/edit/:token",
      to: "services#edit_availabilities",
      as: :edit_availabilities
  patch "/services/availabilities/:token",
        to: "services#update_availabilities",
        as: :update_availabilities
  post "/services/notification_subscriptions/:token",
       to: "notification_subscriptions#create",
       as: :notification_subscriptions
  delete "/services/notification_subscriptions/:token",
         to: "notification_subscriptions#destroy",
         as: :delete_notification_subscriptions

  require 'sidekiq/web'
  require 'sidekiq/cron/web'
  # Sidekiq::Web.set :session_secret, Rails.application.credentials[:secret_key_base]
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    ActiveSupport::SecurityUtils
      .secure_compare(
        ::Digest::SHA256.hexdigest(username),
        ::Digest::SHA256.hexdigest(ENV["SIDEKIQ_USERNAME"])
      ) &
      ActiveSupport::SecurityUtils.secure_compare(
        ::Digest::SHA256.hexdigest(password),
        ::Digest::SHA256.hexdigest(ENV["SIDEKIQ_PASSWORD"])
      )
  end
  mount Sidekiq::Web => '/sidekiq'

  get "/scanner", to: "pages#qrcode_scanner", as: "qrcode_scanner"
  root to: "pages#home"

end
