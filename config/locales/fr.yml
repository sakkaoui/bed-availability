fr:
  title: "Lidispo - l'information interne des disponibilités de lit"

  shared:
    menu:
      menu: "Menu"
      home: "Accueil"
      scanner: "Scanner"
      availabilities: "Disponibilités"
      manager_sign_up: "Créer un établissement"
      manager_login: "Se connecter"
      manager_out: "Se déconnecter"
      entity_qrcode: "QRCode statistiques globales"
      services: "Services"
      service: "Service"
      service_qrcode: "QRCode du service"
      service_availabilities: "Mettre à jour les disponibilités"
      managers: "Responsables"
    auth_links:
      sign_in: "Se connecter"
      forgot_password: "J'ai oublié mon mot de passe"

  pages:
    qrcode_scanner:
      title: "Scannez un QR Code"
      loading: "🎥 Impossible d'acceder au flux vidéo."
    home:
      title: "Lidispo"
      emergencies_description: "Ce service permet aux urgentistes d'avoir une visualisation la plus temps réel possible des lits disponibles dans les services de soins de leur établissement. Une interface qui leur est dédiée affiche ces disponibilités en fonction de la typologie de lit (homme, femme, en isolement ou indifférencié) et par services."
      services_description: "Les membres des services de soins sont encouragés à remplir le plus souvent possible les disponibilités de leur service. Une interface spéciale leur permet en un instant de mettre à jour le nombre de lits vacants et prêts à recevoir un nouveau patient."
      colophon: "Cette application a été développée pour répondre à un besoin courant, mais exacerbé par la crise sanitaire liée au Coronavirus. Le projet est à l'initiative des membres de l'hopital Nicolas Beaujon (Clichy) et de l'AP-HP, et bénévolement développé par Stéphane Akkaoui avec l'aide de Damien Risterucci. L'icone Hospital Bed crée par BomSymbols de The Noun Project."

  entities:
    qrcode:
      explanation: "Vous pouvez imprimer cette page et la disposer dans un endroit non public du service des urgences pour rendre accessible à tous la page de disponibilité de l'établissement."
      emergency_qrcode: "QRCode vers les disponibilités globales"
    availabilities:
      old_record: "Services n'ayant pas mis à jour leurs disponibilités depuis plus de 3 heures."
      recent_record: "Services ayant pas mis à jour leurs disponibilités depuis moins de 30 minutes."
      floor_summary: "Récapitulatif d'étage %{floor}"
      available_single: Lits disponibles en chambre simple
      available_gender_free: Lits disponibles en chambre double indifférenciée
      available_women: Lits disponibles en chambre double femme
      available_men: Lits disponibles en chambre double homme
      floor: "(étage : %{floor})"
      availabilities: "Disponibilité déclarée de l'établissement à %{time}"
      services: "Disponibilités par services"
      global_stats: "Disponibilités globales"
    new:
      title: "Création d'un nouvel établissement"
      errors_count:
        one: '1 erreur dans ce formulaire'
        other: '%{count} erreurs dans ce formulaire'
      name: "Nom de l'établissement"
      manager: "Responsable de l'établissement"
      submit: "Créer l'établissement"
    flash:
      create:
        success: 'Établissement créé avec succès'
        error: 'Impossible de créer cet établissement'

  managers:
    new:
      delete: "Supprimer"
      why_not: "Êtes vous sur de vouloir supprimer ce manager ?"
      title: "Responsables de %{name}"
      list_title: "Responsables existants"
      invite_title: "Inviter un responsable d'établissement"
      errors_count:
        one: '1 erreur dans ce formulaire'
        other: '%{count} erreurs dans ce formulaire'
      email: "Email du responsable :"
      submit: "Envoyer l'invitation"
      name: "Nom du responsable :"
    fields:
      name: "Nom :"
      email: "Email :"
      password: "Mot de passe :"
      password_confirmation: "Confirmation de mot de passe :"
    flash:
      destroy:
        success: 'La suppression du responsable a bien été prise en compte.'
        error: "Impossible de supprimer ce responsable."
      create:
        success: 'Invitation envoyé à ce responsable'
        error: "Impossible d'envoyer cet invitation"

  manager_sessions:
    new:
      title: "Authentification"
      errors_count:
        one: '1 erreur dans ce formulaire'
        other: '%{count} erreurs dans ce formulaire'
      email: "Email :"
      password: "Mot de passe :"
      submit: "Se connecter"
    flash:
      sign_in:
        success: "Vous êtes maintenant connecté"
        error: "Email ou mot de passe incorrect"
      sign_out:
        success: "Vous êtes maintenant déconnecté"

  manager_invitations:
    new:
      title: "Completez votre profil"
      errors_count:
        one: '1 erreur dans ce formulaire'
        other: '%{count} erreurs dans ce formulaire'
      email: "Email :"
      password: "Mot de passe :"
      submit: "Finaliser l'invitation"
    flash:
      update:
        success: "Vous êtes maintenant connecté"
        error: "Email ou mot de passe incorrect"
        bad_token: "Invitation non valide ou déjà utilisée"

  sessions:
    flash:
      must_be_connected: "Vous devez être connecté"

  passwords:
    new:
      title: "J'ai oublié mon mot de passe"
      email: "Entrez votre email de connection"
      submit: "Envoyez moi les instructions de réinitialisation de mon mot de passe"
    edit:
      title: "Réinitialisation de votre mot de passe"
      password: "Nouveau mot de passe :"
      password_confirmation: "Comfirmation du mot de passe"
      submit: "Changer mon mot de passe"
    flash:
      create:
        no_email: "Compte utilateurs introuvable. Vérfiez votre email"
        success: "Un email contenant les informations de réinitialisation vous a été envoyé"
      update:
        bad_token: "Token de réinitialisation invalide"
        success: "Mot de passe mis à jour avec succès"
        error: "Impossible de mettre à jour ce mot de passe"

  services:
    show:
      service_title: "Service : %{service}"
      last_update_html: "Dernière mise à jour : <span>%{time}</span>"
      service_stats: "Disponibilités du service"
      information: "Informations du service"
      floor: "Etage"
      comment: "Autres informations"
      contact: "Autres informations de contact"
      referees: "Liste des référents"
      uds: "UDS"
      total_bed_count: "Nombre de lits existants"
    qrcode:
      explanation: "Vous pouvez imprimer cette page et la disposer dans un endroit non public du service concerné pour rendre accessible à tous la page de mise à jour de ce service."
      emergency_qrcode: "QRCode vers la mise à jour du service"
    edit_availabilities:
      subscribe: "Souscrire"
      new_subscription_title: "Souscrire aux notifications email pour les rappels de mise à jour de ce service :"
      current_subscription: "Vous recevez les notifications sur l'email suivant : %{email}"
      unsubscribe: "Se désabonner"
      see_info: "Voir les informations"
      no_changes: "Mettre à jour sans changement"
      unsubscribe_why_not: "Êtes vous sur de vouloir supprimer cette abonnement ?"
      errors_count:
        zero: "Il y a %{count} erreur"
        one: "Il y a %{count} erreur"
        other: "Il y a %{count} erreurs"
      service_title: "Service : %{service}"
      last_update_html: "Dernière mise à jour : <span>%{time}</span>"
      total_beds_html: "Nombre de lits existants : <span>%{count}</span>"
      available_count_confirmation_html: "Parmi ces <span id='available-count-confirmation'>0</span> lits disponibles, indiquez la répartition : "
      verification_errors: "Il y a une erreur dans les information founies."
      next: Suivant
      previous: "Précédent"
      save: "Mettre à jour"
    index:
      title: "Liste des services de l'établissement"
      add_service: "Ajouter un service"
      floor: "Étage"
      name: "Nom"
      comment: "Autres informations"
      service_referees: "Référents"
      total_bed_count: "Nombre de lits existants"
      uds: "UDS"
      actions: "Actions"
      edit: "Modifier"
      delete: "Supprimer"
      why_not: "Êtes vous sur de vouloir supprimer ce service ?"
      qrcode: "QRCode"
      manage_referees: "Gérer les référents"
      form: "Formulaire référent"
    new:
      title: "Ajouter un service"
      submit: "Créer le service"
    edit:
      title: "Modifier un service"
      errors_count:
        one: '1 erreur dans ce formulaire'
        other: '%{count} erreurs dans ce formulaire'
      submit: "Mettre à jour le service"
    fields:
      name: "Nom :"
      floor: "Étage :"
      total_bed_count: "Nombre de lits existants :"
      comment: "Autres informations :"
      contact: "Autres informations de contact :"
      uds: 'UDS :'
    flash:
      availabilities:
        success: "La mise à jour des disponibilités du service à bien été prise en compte."
        error: "Impossible de mettre à jour le service."
      create:
        success: "Service créé avec succès."
        error: "Impossible de créer ce service."
      update:
        success: "Service mis à jours avec succès."
        error: "impossible de mettre à jour ce service."
      destroy:
        success: "Service supprimé."
        error: "Impossible de supprimer ce service."

  service_referees:
    new:
      delete: "Supprimer"
      why_not: "Êtes vous sur de vouloir supprimer ce référent ?"
      title: "Inviter un référent"
      referees_title: "Référents existants :"
      name: "Nom : "
      email: "Email : "
      contact_details: "Infrmations de contact :"
      submit: "Envoyer l'invitation"
      errors_count:
        one: '1 erreur dans ce formulaire.'
        other: '%{count} erreurs dans ce formulaire.'
    flash:
      create:
        already_exists: "Ce référent est déjà dans ce service."
        success: 'Invitation envoyé à ce référent.'
        error: "Impossible d'envoyer cet invitation."
      destroy:
        success: 'La suppression du référent a bien été prise en compte.'
        error: "Impossible de supprimer ce référent."

  notification_subscriptions:
    flash:
      create:
        already_exist: "Cet email est déjà abonné aux notification pour ce service."
        success: "L'abonnement a bien été pris en compte."
        errors: "Impossible d'enregistrer cet abonnement : %{errors}."
      destroy:
        success: "L'abonnement a bien été résilé."
        errors: "Impossible de se désabonner."
  assignations:
    flash:
      notifications_false:
        success: "Vous ne recevrez plus de notification pour ce service."
        error: "Impossible de vous désabonner."
      notifications_true:
        success: "Vous êtes maintenant abonné aux notifications pour ce service."
        error: "Impossible de vous abonner."