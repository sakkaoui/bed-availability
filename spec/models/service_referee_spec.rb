require 'rails_helper'

RSpec.describe ServiceReferee, type: :model do

  describe "Attributes" do
    it { is_expected.to have_db_column(:name).of_type(:string) }
    it { is_expected.to have_db_column(:email).of_type(:string) }
    it { is_expected.to have_db_column(:password_digest).of_type(:string) }
  end

  describe "Validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:password_digest) }

    describe "email" do
      subject { FactoryBot.create(:service_referee) }

      it { is_expected.to validate_presence_of(:email) }

      it do
        is_expected.to validate_uniqueness_of(:email).ignoring_case_sensitivity
      end

      describe "format" do
        it { is_expected.to allow_value("test@gmail.com").for(:email) }
        it { is_expected.to allow_value("test-hyphen@gmail.com").for(:email) }
        it { is_expected.to allow_value("test.dot@gmail.com").for(:email) }
        it { is_expected.to allow_value("test-13-digit@gmail.com").for(:email) }
        it { is_expected.to allow_value("test@gmail.co.uk").for(:email) }
        it { is_expected.to allow_value("test+suffix@gmail.com").for(:email) }

        it { is_expected.not_to allow_value("not an email").for(:email) }
        it { is_expected.not_to allow_value("gmail.com").for(:email) }
        it { is_expected.not_to allow_value("test@gmail").for(:email) }
        it { is_expected.not_to allow_value("test@gmail@com").for(:email) }
      end

      it "should store email as lowercase" do
        u = FactoryBot.create(:manager, email: "MiXeDCaSe@aphp.com")
        expect(u.reload.email).to eq("mixedcase@aphp.com")
      end
    end
  end

  describe 'Relations' do
    it { is_expected.to have_many(:assignations).dependent(:destroy) }
    it { is_expected.to belong_to(:entity).required(true) }

    it do
      is_expected
        .to have_many(:services)
        .through(:assignations).inverse_of(:service_referees)
    end
  end
end
