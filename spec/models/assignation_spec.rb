require 'rails_helper'

RSpec.describe Assignation, type: :model do

  describe "Attributes" do
    it { is_expected.to have_db_column(:service_id).of_type(:integer) }
    it { is_expected.to have_db_column(:service_referee_id).of_type(:integer) }
    it { is_expected.to have_db_column(:accept_notifications).of_type(:boolean) }
  end

  describe "Validations" do
    it { is_expected.to validate_presence_of(:service) }
    it { is_expected.to validate_presence_of(:service_referee) }
  end

  describe "Associations" do
    it { is_expected.to belong_to(:service) }
    it { is_expected.to belong_to(:service_referee) }
  end

end
