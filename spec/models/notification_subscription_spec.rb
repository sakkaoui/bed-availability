require 'rails_helper'

RSpec.describe NotificationSubscription, type: :model do

  describe "Attributes" do
    it { is_expected.to have_db_column(:email).of_type(:string) }
    it { is_expected.to have_db_column(:service_id).of_type(:integer) }
  end

  describe "Validations" do
    describe "email" do
      subject { FactoryBot.create(:notification_subscription) }

      it { is_expected.to validate_presence_of(:email) }

      describe "format" do
        it { is_expected.to allow_value("test@gmail.com").for(:email) }
        it { is_expected.to allow_value("test-hyphen@gmail.com").for(:email) }
        it { is_expected.to allow_value("test.dot@gmail.com").for(:email) }
        it { is_expected.to allow_value("test-13-digit@gmail.com").for(:email) }
        it { is_expected.to allow_value("test@gmail.co.uk").for(:email) }
        it { is_expected.to allow_value("test+suffix@gmail.com").for(:email) }

        it { is_expected.not_to allow_value("not an email").for(:email) }
        it { is_expected.not_to allow_value("gmail.com").for(:email) }
        it { is_expected.not_to allow_value("test@gmail").for(:email) }
        it { is_expected.not_to allow_value("test@gmail@com").for(:email) }
      end

      it "should store email as lowercase" do
        u = FactoryBot.create(:manager, email: "MiXeDCaSe@aphp.com")
        expect(u.reload.email).to eq("mixedcase@aphp.com")
      end
    end
  end

  describe 'Relations' do
    it do
      is_expected
        .to belong_to(:service)
        .required(true).inverse_of(:notification_subscriptions)
    end
  end
end
