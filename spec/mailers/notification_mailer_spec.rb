require "rails_helper"

RSpec.describe NotificationMailer, type: :mailer do

  describe "#notify" do
    let(:service) do
      FactoryBot.create(:service)
    end
    let(:emails) do
      [Faker::Internet.email]
    end

    let(:mail) do
      described_class.with(
        emails: emails,
        service: FactoryBot.create(:service)
      ).notify
    end

    it "should be sent to emails" do
      expect(mail.bcc).to eq(emails)
    end
  end

end
