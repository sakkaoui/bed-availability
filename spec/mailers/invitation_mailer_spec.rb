require "rails_helper"

RSpec.describe InvitationMailer, type: :mailer do

  describe "#create_referee" do
    let(:manager) do
      FactoryBot.create(:manager)
    end

    let(:mail) do
      described_class.with(
        manager: manager
      ).create_referee
    end

    it "should be send to manager" do
      expect(mail.to).to eq([manager.email])
    end

    it "should not contain invite link" do
      expect(mail.body.encoded)
        .to include(ERB::Util.html_escape(manager.invitation_token))
    end
  end

  describe "#add_referee" do
    let(:referee) do
      FactoryBot.create(:service_referee)
    end

    let(:mail) do
      described_class.with(
        service_referee: referee,
        service: FactoryBot.create(:service)
      ).add_referee
    end

    it "should be sent to manager" do
      expect(mail.to).to eq([referee.email])
    end

    it "should not contain invite link" do
      expect(mail.body.encoded)
        .not_to include(ERB::Util.html_escape(referee.invitation_token))
    end
  end

end
