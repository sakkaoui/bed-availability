require "rspec/rails"
require "factory_bot_rails"

# Preview all emails at http://localhost:3000/rails/mailers/transactional_mailer
class NotificationMailerPreview < ActionMailer::Preview

  def notify
    emails = [Faker::Internet.email, Faker::Internet.email]
    service = FactoryBot.create(:service)
    NotificationMailer.with(
      service: service,
      emails: emails
    ).notify
  end

end
