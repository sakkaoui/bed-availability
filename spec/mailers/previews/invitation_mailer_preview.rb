require "rspec/rails"
require "factory_bot_rails"

# Preview all emails at http://localhost:3000/rails/mailers/transactional_mailer
class InvitationMailerPreview < ActionMailer::Preview

  def create_referee
    InvitationMailer.create_referee(FactoryBot.create(:manager).id)
  end

  def add_referee
    InvitationMailer.with(
      service_referee: FactoryBot.create(:service_referee),
      service: FactoryBot.create(:service)
    ).add_referee
  end

end
