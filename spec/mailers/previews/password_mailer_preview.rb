require "rspec/rails"
require "factory_bot_rails"

# Preview all emails at http://localhost:3000/rails/mailers/transactional_mailer
class PasswordMailerPreview < ActionMailer::Preview

  def reset
    PasswordMailer.with(
      manager_id: FactoryBot.create(:manager, :ask_reset),
    ).reset
  end

end
