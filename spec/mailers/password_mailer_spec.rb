require 'rails_helper'

RSpec.describe PasswordMailer, type: :mailer do

  describe '#reset' do
    let(:manager) { FactoryBot.create(:manager, :ask_reset) }
    let(:mail) { described_class.with(manager_id: manager.id).reset }

    it 'must be send to email' do
      expect(mail.to).to eq([manager.email])
    end

    it 'must contain a reset link' do
      expect(mail.body.encoded)
        .to include(ERB::Util.html_escape(manager.password_reset_token))
    end
  end

end
