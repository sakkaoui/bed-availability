FactoryBot.define do
  factory :manager do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password { Faker::Alphanumeric.alpha }
    entity

    trait :invited do
      invitation_sent_at { 1.day.ago }
      password_digest { 'passw0rd' }
    end

    trait :ask_reset do
      after(:build) { |manager| manager.generate_password_reset_token }
    end
  end
end
