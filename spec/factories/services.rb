FactoryBot.define do
  factory :service do
    name { Faker::Name.name }
    floor { 1 }

    total_bed_count { 10 }
    available_women_bed_count { 4 }
    available_man_bed_count { 2 }
    available_gender_free_bed_count { 1 }
    available_isolated_bed_count { 1 }
    current_availability { 8 }
    planned_arrivals_count { 1 }
    planned_leaves_count { 1 }

    comment { Faker::Lorem.paragraph }

    entity
  end
end
