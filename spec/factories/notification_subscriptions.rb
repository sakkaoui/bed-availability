FactoryBot.define do

  factory :notification_subscription do
    email { Faker::Internet.email }
    service
  end

end
