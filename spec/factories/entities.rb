FactoryBot.define do
  factory :entity do
    name { "#{Faker::Company.name} #{%w[Care Hospital Center].sample}" }
  end
end
