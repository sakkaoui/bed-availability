FactoryBot.define do
  factory :service_referee do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password_digest { Faker::Alphanumeric.alpha }
    contact_details { Faker::Lorem.paragraph }

    entity
  end
end
