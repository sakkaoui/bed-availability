require 'rails_helper'

RSpec.describe ManagersController, type: :controller do

  describe "#new" do
    let(:get_request) { get :new }

    describe 'authenticated' do
      let(:manager) { FactoryBot.create(:manager) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        get_request
      end

      it 'must render new template' do
        get_request
        expect(response).to render_template(:new)
      end

      it "should assigns a manager" do
        get_request
        expect(assigns(:manager)).not_to be_nil
      end

      it "should assigns a manager for the current entity" do
        get_request
        expect(assigns(:manager).entity).to eq(manager.entity)
      end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe "#create" do
    let(:manager_attributes) do
      {
        email: Faker::Internet.email,
        name: Faker::Name.name
      }
    end
    let(:post_request) do
      post :create, params: {
        manager: manager_attributes
      }
    end

    describe 'authenticated' do
      let!(:manager) { FactoryBot.create(:manager) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        post_request
      end

      describe "with valid parameters" do
        it 'must render new template' do
          post_request
          expect(response).to redirect_to(root_url)
        end

        it "should create a manager" do
          expect do
            post_request
          end.to change { Manager.count }.by(1)
        end

        it "should create a manager for the targeted entity" do
          expect do
            post_request
          end.to change { manager.entity.reload.managers.count }.by(1)
        end

        it "should save given fields" do
          post_request
          new_manager = Manager.where.not(id: manager.id).last
          expect(new_manager.email).to eq(manager_attributes[:email])
          expect(new_manager.name).to eq(manager_attributes[:name])
        end
      end

      describe "with invalid params" do
        let(:manager_attributes) do
          {
            email: Faker::Internet.email,
            name: nil
          }
        end

        it 'must render new' do
          post_request
          expect(response).to render_template(:new)
        end

        it "should not create a manager" do
          expect do
            post_request
          end.not_to change { Manager.count }
        end

        it "should not create a manager for the targeted entity" do
          expect do
            post_request
          end.not_to change { manager.entity.reload.managers.count }
        end
      end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        post_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe "#destroy" do
    let(:manager) { FactoryBot.create(:manager) }

    describe 'authenticated' do

      before(:each) { authenticate(manager.auth_token, controller) }

      describe "with existing manager from same entity" do
        let!(:manager_to_be_destroyed) do
          FactoryBot.create(:manager, entity: manager.entity)
        end

        it "should destroy the manager" do
          expect(Manager.where(id: manager_to_be_destroyed.id))
            .to exist
          delete :destroy, params: { id: manager_to_be_destroyed.id }
          expect(Manager.where(id: manager_to_be_destroyed.id))
            .not_to exist
        end

        it "should redirect to manager list" do
          delete :destroy, params: { id: manager_to_be_destroyed.id }
          expect(response).to redirect_to(new_manager_path)
        end
      end

      describe "with existing manager from another enity" do
        let!(:manager_to_be_destroyed) { FactoryBot.create(:manager) }

        it "should not destroy the manager" do
          expect(Manager.where(id: manager_to_be_destroyed.id))
            .to exist
          delete :destroy, params: { id: manager_to_be_destroyed.id }
          expect(Manager.where(id: manager_to_be_destroyed.id))
            .to exist
        end

        it "should redirect to manager list" do
          delete :destroy, params: { id: manager_to_be_destroyed.id }
          expect(response).to redirect_to(new_manager_path)
        end
      end

      describe "with bad id" do
        it "should not destroy any manager" do
          expect do
            delete :destroy, params: { id: "fake-id" }
          end.not_to change { Manager.count }
        end

        it "should redirect to manager list" do
          delete :destroy, params: { id: "fake-id" }
          expect(response).to redirect_to(new_manager_path)
        end
      end
    end

    describe "unauthenticated" do
      it "should redirect to /" do
        delete :destroy, params: { id: "fake-id" }
        expect(response).to redirect_to(root_url)
      end
    end
  end

end
