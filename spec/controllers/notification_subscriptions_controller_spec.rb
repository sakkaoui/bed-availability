require 'rails_helper'

RSpec.describe NotificationSubscriptionsController, type: :controller do

  describe "#create" do
    let(:service) { FactoryBot.create(:service) }
    let(:sub_params) { { email: Faker::Internet.email } }

    describe "with no email in params" do
      let(:sub_params) { { email: "" } }

      it "should not create a subscription" do
        expect do
          post :create, params: {
            token: service.token,
            notification_subscription: sub_params
          }
        end.not_to change { NotificationSubscription.count }
      end

      it "should redirect to edit availabilities" do
        post :create, params: {
          token: service.token,
          notification_subscription: sub_params
        }
        expect(response)
          .to redirect_to(edit_availabilities_url(token: service.token))
      end

      it "should not set cookies" do
        post :create, params: {
          token: service.token,
          notification_subscription: sub_params
        }
        expect(cookies.signed[:notification_subscription_id]).to be_nil
      end
    end

    describe "with existing email for this service" do
      let!(:existing_sub) do
        FactoryBot.create(
          :notification_subscription,
          service: service,
          email: sub_params[:email]
        )
      end

      it "should not create a subscription" do
        expect do
          post :create, params: {
            token: service.token,
            notification_subscription: sub_params
          }
        end.not_to change { NotificationSubscription.count }
      end

      it "should redirect to edit availabilities" do
        post :create, params: {
          token: service.token,
          notification_subscription: sub_params
        }
        expect(response)
          .to redirect_to(edit_availabilities_url(token: service.token))
      end
    end

    describe "with exiting email for another service" do
      let!(:existing_sub) do
        FactoryBot.create(
          :notification_subscription,
          service: FactoryBot.create(:service),
          email: sub_params[:email]
        )
      end

      it "should create a subscription" do
        expect do
          post :create, params: {
            token: service.token,
            notification_subscription: sub_params
          }
        end.to change { NotificationSubscription.count }.by(1)
      end

      it "should set cookies" do
        # expect(an_instance_of(NotificationSubscriptionsController))
        #   .to receive(:set_notification_subscription_id)
        post :create, params: {
          token: service.token,
          notification_subscription: sub_params
        }
        expect(cookies.signed[:notification_subscription_id])
          .to eq("{\"#{service.id}\":#{NotificationSubscription.last.id}}")
      end

      it "should redirect to edit availabilities" do
        post :create, params: {
          token: service.token,
          notification_subscription: sub_params
        }
        expect(response)
          .to redirect_to(edit_availabilities_url(token: service.token))
      end
    end

    describe "with no existing email" do
      it "should create a subscription" do
        expect do
          post :create, params: {
            token: service.token,
            notification_subscription: sub_params
          }
        end.to change { NotificationSubscription.count }.by(1)
      end

      it "should redirect to edit availabilities" do
        post :create, params: {
          token: service.token,
          notification_subscription: sub_params
        }
        expect(response)
          .to redirect_to(edit_availabilities_url(token: service.token))
      end
    end
  end

  describe "#destroy" do
    let(:service) { FactoryBot.create(:service) }

    describe "with existing notification" do
      let!(:notif) do
        FactoryBot.create(:notification_subscription, service: service)
      end

      it "should destroy the notification" do
        expect do
          delete :destroy, params: { token: service.token, id: notif.id }
        end.to change { NotificationSubscription.count }.by(-1)
      end

      it "should redirect to edit availabilities" do
        delete :destroy, params: { token: service.token, id: notif.id }
        expect(response).to redirect_to(edit_availabilities_path(service.token))
      end

      it "should empty the service cookies" do
        cookies.signed[:notification_subscription_id] =
          "{\"#{service.id}\":#{notif.id}}"
        delete :destroy, params: { token: service.token, id: notif.id }
        expect(cookies.signed[:notification_subscription_id])
          .to eq("{}")
      end
    end

    describe "with notification for another service" do
      let!(:notif) do
        FactoryBot.create(
          :notification_subscription,
          service: FactoryBot.create(:service, entity: service.entity)
        )
      end

      it "should destroy the notification" do
        expect do
          delete :destroy, params: { token: service.token, id: notif.id }
        end.not_to change { NotificationSubscription.count }
      end

      it "should redirect to edit availabilities" do
        delete :destroy, params: { token: service.token, id: notif.id }
        expect(response).to redirect_to(edit_availabilities_path(service.token))
      end
    end

    describe "with wrong service token" do
      let(:notif) { FactoryBot.create(:notification_subscription) }

      it "should redirect to root" do
        delete :destroy, params: { token: "fake-token", id: notif.id }
        expect(response).to redirect_to(root_url)
      end
    end
  end

end
