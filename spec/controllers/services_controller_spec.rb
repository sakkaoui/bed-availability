require 'rails_helper'

RSpec.describe ServicesController, type: :controller do

  describe "#edit_avalabilities" do
    describe "with valid token" do
      let(:service) { FactoryBot.create(:service) }
      let(:get_request) do
        get :edit_availabilities, params: { token: service.token }
      end

      it "should render edit_availabilities template" do
        get_request
        expect(response).to render_template(:edit_availabilities)
      end

      it "should find corresponding service" do
        get_request
        expect(assigns(:service)).to eq(service)
      end
    end

    describe "with invalid token" do
      let(:get_request) do
        get :edit_availabilities, params: { token: "fake-token" }
      end

      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe "#update_avaliabilities" do
    describe "with valid token" do
      let(:service) { FactoryBot.create(:service) }

      describe "with valid parameters" do
        let(:valid_parameters) do
          {
            current_availability: 2,
            planned_leaves_count: 2,
            planned_arrivals_count: 1,
            available_women_bed_count: 1,
            available_man_bed_count: 1,
            available_gender_free_bed_count: 0,
            available_isolated_bed_count: 1
          }
        end
        let(:patch_request) do
          patch :update_availabilities, params: {
            token: service.token,
            service: valid_parameters
          }
        end

        it "should render edit_availabilities template" do
          patch_request
          expect(response).to redirect_to(:edit_availabilities)
        end

        it "should find corresponding service" do
          patch_request
          expect(assigns(:service)).to eq(service)
        end

        it "should update service" do
          patch_request
          service.reload
          expect(service.available_women_bed_count)
            .to eq(valid_parameters[:available_women_bed_count])
          expect(service.available_man_bed_count)
            .to eq(valid_parameters[:available_man_bed_count])
          expect(service.available_gender_free_bed_count)
            .to eq(valid_parameters[:available_gender_free_bed_count])
          expect(service.available_isolated_bed_count)
            .to eq(valid_parameters[:available_isolated_bed_count])
        end

        it "should update service timestamps even without changes" do
          old_updated_at = service.updated_at
          patch :update_availabilities, params: {
            token: service.token,
            service: service.attributes
          }
          expect(old_updated_at < service.reload.updated_at).to be_truthy
        end
      end

      describe "with invalid parameters" do
        let(:valid_parameters) do
          {
            current_availability: 2,
            planned_leaves_count: 2,
            planned_arrivals_count: 1,
            available_women_bed_count: 1,
            available_man_bed_count: 1,
            available_gender_free_bed_count: 19,
            available_isolated_bed_count: 1
          }
        end
        let(:patch_request) do
          patch :update_availabilities, params: {
            token: service.token,
            service: valid_parameters
          }
        end

        it "should render edit_availabilities template" do
          patch_request
          expect(response).to render_template(:edit_availabilities)
        end

        it "should find corresponding service" do
          patch_request
          expect(assigns(:service)).to eq(service)
        end

        it "should not update service" do
          old_service = service.dup
          patch_request
          service.reload
          expect(service.available_women_bed_count)
            .to eq(old_service.available_women_bed_count)
          expect(service.available_man_bed_count)
            .to eq(old_service.available_man_bed_count)
          expect(service.available_gender_free_bed_count)
            .to eq(old_service.available_gender_free_bed_count)
          expect(service.available_isolated_bed_count)
            .to eq(old_service.available_isolated_bed_count)
        end
      end
    end

    describe "with invalid token" do
      let(:patch_request) do
        patch :update_availabilities, params: { token: "fake-token" }
      end

      it "should redirect to home" do
        patch_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe "#qrcode" do
    describe "with valid token" do
      let(:service) { FactoryBot.create(:service) }
      let(:get_request) { get :qrcode, params: { token: service.token } }

      it "should render availabilities template" do
        get_request
        expect(response).to render_template(:qrcode)
      end

      it "should find corresponding entity" do
        get_request
        expect(assigns(:service)).to eq(service)
      end
    end

    describe "with invalid token" do
      let(:get_request) { get :qrcode, params: { token: "fake-token" } }

      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe "#show" do
    describe "with valid token" do
      let(:service) { FactoryBot.create(:service) }
      let(:get_request) do
        get :show, params: { id: service.id, token: service.entity.token }
      end

      it "should render availabilities template" do
        get_request
        expect(response).to render_template(:show)
      end

      it "should find corresponding entity" do
        get_request
        expect(assigns(:service)).to eq(service)
      end
    end

    describe "with other entity service id" do
      let(:service) { FactoryBot.create(:service) }
      let(:other_service) { FactoryBot.create(:service) }
      let(:get_request) do
        get :show, params: { id: other_service.id, token: service.entity.token }
      end

      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end

    describe "with invalid entity token" do
      let(:service) { FactoryBot.create(:service) }
      let(:get_request) do
        get :show, params: { id: service.id, token: "fake-token" }
      end

      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe '#index' do
    let(:entity) { FactoryBot.create(:entity) }
    let!(:services) { FactoryBot.create_list(:service, 3, entity: entity) }
    let(:get_request) { get :index }

    describe 'authenticated' do
      let(:manager) { FactoryBot.create(:manager, entity: entity) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        get_request
      end

      it 'must render index template' do
        get_request
        expect(response).to render_template(:index)
      end

      it "must find corresponding entities" do
        get_request
        expect(assigns(:services).count).to eq(3)
      end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe '#new' do
    let(:get_request) { get :new }

    describe 'authenticated' do
      let(:manager) { FactoryBot.create(:manager) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        get_request
      end

      it 'must render index template' do
        get_request
        expect(response).to render_template(:new)
      end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe '#create' do
    let(:post_request) { post :create, params: params }

    describe 'authenticated' do
      let(:params) do
        {
          service: {
            name: "Test",
            floor: 2,
            total_bed_count: 12,
            comment: 'TOtotototo',
          },
        }
      end
      let(:manager) { FactoryBot.create(:manager) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        post_request
      end

      describe 'with valid params' do
        it 'must not have errors on services' do
          post_request
          expect(assigns(:service).errors).to be_empty
        end

        it 'must redirect_to index template' do
          post_request
          expect(response).to redirect_to(services_url)
        end
      end

      describe 'with no name in params' do
        let(:params) do
          {
            service: {
              floor: 2,
              total_bed_count: 12,
              comment: 'TOtotototo',
            },
          }
        end

        it 'must not have errors on services' do
          post_request
          expect(assigns(:service).errors).not_to be_empty
        end

        it 'must have errors on name' do
          post_request
          expect(assigns(:service).errors[:name].count).to be(1)
        end
      end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        post :create, params: {}
        expect(response).to redirect_to('/')
      end
    end
  end

  describe '#edit' do
    let(:service) { FactoryBot.create(:service) }
    let(:get_request) { get :edit, params: { id: service.id } }

    describe 'authenticated' do
      let(:manager) { FactoryBot.create(:manager, entity: service.entity) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        get_request
      end

      it 'must render index template' do
        get_request
        expect(response).to render_template(:edit)
      end

      it 'must assign a service' do
        get_request
        expect(assigns(:service)).to eq(service)
      end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe '#update' do
    let(:service) { FactoryBot.create(:service) }
    let(:patch_request) do
      patch :create, params: { id: service.id }.merge(params)
    end

    describe 'authenticated' do
      let(:params) do
        {
          service: {
            name: "Test",
            floor: 2,
            total_bed_count: 12,
            comment: 'TOtotototo',
          },
        }
      end
      let(:manager) { FactoryBot.create(:manager) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        patch_request
      end

      describe 'with valid params' do
        it 'must not have errors on services' do
          patch_request
          expect(assigns(:service).errors).to be_empty
        end

        it 'must redirect_to index template' do
          patch_request
          expect(response).to redirect_to(services_url)
        end
      end

      describe 'with no name in params' do
        let(:params) do
          {
            service: {
              name: nil,
              floor: 2,
              total_bed_count: 12,
              comment: 'TOtotototo',
            },
          }
        end

        it 'must not have errors on services' do
          patch_request
          expect(assigns(:service).errors).not_to be_empty
        end

        it 'must have errors on name' do
          patch_request
          expect(assigns(:service).errors[:name].count).to be(1)
        end
      end

      # describe 'with bad service id', focus: true do
      #   it "should redirect to home" do
      #     patch :update, params: { id: 0 }
      #     expect(response).to redirect_to('/')
      #   end
      # end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        patch :update, params: { id: service.id }
        expect(response).to redirect_to('/')
      end
    end
  end

  describe '#delete' do
    let!(:service) { FactoryBot.create(:service) }
    let(:delete_request) { delete :destroy, params: { id: service.id } }

    describe 'authenticated' do
      let(:manager) { FactoryBot.create(:manager, entity: service.entity) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        delete_request
      end

      describe 'with valid params' do
        it 'must delete a service' do
          expect { delete_request } .to change { Service.count }.by(-1)
        end

        it 'must redirect_to index template' do
          delete_request
          expect(response).to redirect_to(services_url)
        end
      end

      describe 'with bad id' do
        let(:delete_request) { delete :destroy, params: { id: 0 } }

        it 'must not delete a service' do
          expect { delete_request }.not_to change { Service.count }
        end
      end
    end

    describe 'unauthenticated' do
      it 'should redirect to home' do
        delete :destroy, params: { id: service.id }
        expect(response).to redirect_to('/')
      end

      it 'must not delete a service' do
        expect { delete_request }.not_to change { Service.count }
      end
    end
  end

end
