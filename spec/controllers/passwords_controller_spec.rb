require 'rails_helper'

RSpec.describe PasswordsController, type: :controller do

  describe '#new' do
    let(:manager) { FactoryBot.create(:manager) }
    let(:get_request) do
      get(:new, params: {})
    end

    it 'must not require authentication' do
      allow(controller).to receive(:require_login)

      get_request
      expect(controller).not_to have_received(:require_login)
    end

    it 'must respond in html by default' do
      get_request
      expect(response.content_type).to eq('text/html; charset=utf-8')
    end

    it 'must render new' do
      get_request
      expect(response).to render_template(:new)
    end
  end

  describe '#create' do
    let(:manager) { FactoryBot.create(:manager) }
    let(:post_request) do
      post(:create, params: { email: manager.email })
    end

    it 'must not require authentication' do
      allow(controller).to receive(:require_login)

      post_request
      expect(controller).not_to have_received(:require_login)
    end

    it 'must respond in html by default' do
      post_request
      expect(response.content_type).to eq('text/html; charset=utf-8')
    end

    it 'must redirect to sign_in' do
      post_request
      expect(response).to redirect_to(sign_in_managers_url)
    end

    it 'must generate a new password token' do
      expect { post_request }.to change { manager.reload.password_reset_token }
    end

    it 'must set a date until validation' do
      travel_to Time.zone.now do
        post_request
        expect(manager.reload.password_reset_valid_until)
          .to eq((Time.current + 1.hour).to_i)
      end
    end

    describe 'with blank email' do
      let(:post_request) do
        post(:create, params: { email: Faker::Internet.email })
      end

      it 'must render new' do
        post_request
        expect(response).to render_template(:new)
      end

      it 'must not generate a new password token' do
        expect { post_request }
          .not_to change { manager.reload.password_reset_token }
      end

      it 'must not set a date until validation' do
        expect { post_request }
          .not_to change { manager.reload.password_reset_valid_until }
      end
    end

    describe 'with invalid email' do
      let(:post_request) do
        post(:create, params: { email: Faker::Internet.email })
      end

      it 'must render new' do
        post_request
        expect(response).to render_template(:new)
      end

      it 'must not generate a new password token' do
        expect { post_request }
          .not_to change { manager.reload.password_reset_valid_until }
      end

      it 'must not set a date until validation' do
        expect { post_request }
          .not_to change { manager.reload.password_reset_valid_until }
      end
    end
  end

  describe '#edit' do
    let(:manager) { FactoryBot.create(:manager) }
    let(:get_request) do
      get(:edit, params: { token: manager.password_reset_token })
    end

    it 'must not require authentication' do
      allow(controller).to receive(:require_login)

      get_request
      expect(controller).not_to have_received(:require_login)
    end

    it 'must respond in html by default' do
      get_request
      expect(response.content_type).to eq('text/html; charset=utf-8')
    end

    it 'must render edit' do
      get_request
      expect(response).to render_template(:edit)
    end

    it 'must find the related manager' do
      get_request
      expect(assigns(:manager)).to eq(manager)
    end

    describe 'whith bad token' do
      it 'must redirect to sign_in_url' do
        get(:edit, params: { token: "fake-token" })
        expect(response).to redirect_to(sign_in_managers_url)
      end
    end
  end

  describe('#update') do
    let!(:manager) { FactoryBot.create(:manager, :ask_reset) }

    let(:params) do
      {
        token: manager.password_reset_token,
        password: 'test',
        password_confirmation: 'test',
      }
    end

    let(:patch_request) { patch(:update, params: params) }

    it 'must not require authentication' do
      allow(controller).to receive(:require_login)

      patch_request
      expect(controller).not_to have_received(:require_login)
    end

    describe 'with valid params' do
      it 'must find the related manager' do
        patch_request
        expect(assigns(:manager)).to eq(manager)
      end

      it 'must not have errors on manager' do
        patch_request
        expect(assigns(:manager).errors).to be_empty
      end

      it 'must redirect to root_url' do
        patch_request
        expect(response).to redirect_to(root_url)
      end

      it 'must change password' do
        expect { patch_request }.to change { manager.reload.password_digest }
      end

      it 'must authenticate manager' do
        patch_request
        expect(cookies.signed[:jwt].presence).to eq(manager.auth_token)
      end
    end

    describe 'with invalid params' do
      let(:params) do
        {
          token: 'fake-token',
          manager: {
            password: 'test',
            password_confirmation: 'test',
          }
        }
      end

      it 'must not find the related manager' do
        patch_request
        expect(assigns(:manager)).not_to eq(manager)
      end

      it 'must render edit' do
        patch_request
        expect(response).to render_template(:edit)
      end

      it 'must not change password' do
        expect { patch_request }
          .not_to change { manager.reload.password_digest }
      end

      it 'must not authenticate manager' do
        patch_request
        expect(cookies.signed[:jwt].presence).not_to eq(manager.auth_token)
      end
    end
  end

end
