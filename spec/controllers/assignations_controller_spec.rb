require 'rails_helper'

RSpec.describe AssignationsController, type: :controller do

  describe "#destroy" do
    let(:service) { FactoryBot.create(:service) }

    describe "with login" do
      let!(:user) do
        FactoryBot.create(:service_referee, entity: service.entity)
      end

      before(:each) { authenticate(user.auth_token, controller) }

      it 'should require authentication' do
        expect(controller).to receive(:require_login)
        delete :disable_notifications, params: { service_id: service.id }
      end


      describe "with assignation" do
        let!(:assignation) do
          FactoryBot.create(
            :assignation,
            service: service,
            service_referee: user
          )
        end

        it "should set the notifcation flag to false" do
          delete :disable_notifications, params: { service_id: service.id }
          expect(assignation.reload.accept_notifications?).to be_falsy
        end

        it "should redirect to edit availabilities" do
          delete :disable_notifications, params: { service_id: service.id }
          expect(response).to redirect_to(edit_availabilities_path(service.token))
        end
      end

      describe "without assignation" do
        it "should redirect to edit availabilities" do
          delete :disable_notifications, params: { service_id: service.id }
          expect(response).to redirect_to(edit_availabilities_path(service.token))
        end
      end
    end

    describe "without login" do
      it "should redirect to root" do
        delete :disable_notifications, params: { service_id: service.id }
        expect(response).to redirect_to(root_url)
      end
    end
  end

  describe "#enable_notifications" do
    let(:service) { FactoryBot.create(:service) }

    describe "with login" do
      let!(:user) do
        FactoryBot.create(:service_referee, entity: service.entity)
      end

      before(:each) { authenticate(user.auth_token, controller) }

      it 'should require authentication' do
        expect(controller).to receive(:require_login)
        delete :enable_notifications, params: { service_id: service.id }
      end

      describe "with assignation" do
        let!(:assignation) do
          FactoryBot.create(
            :assignation,
            service: service,
            service_referee: user,
            accept_notifications: false
          )
        end

        it "should set the notifcation flag to false" do
          delete :enable_notifications, params: { service_id: service.id }
          expect(assignation.reload.accept_notifications?).to be_truthy
        end

        it "should redirect to edit availabilities" do
          delete :enable_notifications, params: { service_id: service.id }
          expect(response).to redirect_to(edit_availabilities_path(service.token))
        end
      end

      describe "without assignation" do
        it "should redirect to edit availabilities" do
          delete :enable_notifications, params: { service_id: service.id }
          expect(response).to redirect_to(edit_availabilities_path(service.token))
        end
      end
    end

    describe "without login" do
      it "should redirect to root" do
        delete :enable_notifications, params: { service_id: service.id }
        expect(response).to redirect_to(root_url)
      end
    end
  end

end
