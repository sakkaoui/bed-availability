require 'rails_helper'

RSpec.describe ManagerInvitationsController, type: :controller do

  describe('#new') do
    let(:manager) { FactoryBot.create(:manager, :invited) }
    let(:get_request) do
      get(:new, params: { token: manager.invitation_token })
    end

    it 'must not require authentication' do
      allow(controller).to receive(:require_login)

      get_request
      expect(controller).not_to have_received(:require_login)
    end

    it 'must respond in html by default' do
      get_request
      expect(response.content_type).to eq('text/html; charset=utf-8')
    end

    it 'must find the related manager' do
      get_request
      expect(assigns(:manager)).to eq(manager)
    end

    it 'must render new' do
      get_request
      expect(response).to render_template(:new)
    end

    describe 'whith bad token' do
      it 'must redirect to root_url' do
        get(:new, params: { token: "fake-token" })
        expect(response).to redirect_to(root_url)
      end
    end
  end

  describe('#update') do
    let!(:manager) { FactoryBot.create(:manager, :invited) }

    let(:params) do
      {
        token: manager.invitation_token,
        manager: {
          name: 'Toto',
          password: 'test',
          password_confirmation: 'test',
        }
      }
    end

    let(:patch_request) { patch(:update, params: params) }

    it 'must not require authentication' do
      allow(controller).to receive(:require_login)

      patch_request
      expect(controller).not_to have_received(:require_login)
    end

    describe 'with valid params' do
      it 'must not have errors on manager' do
        patch_request
        expect(assigns(:manager).errors).to be_empty
      end

      it 'must set invitation_token to nil' do
        expect { patch_request }.to(
          change { manager.reload.invitation_token }
            .from(manager.invitation_token).to(nil),
        )
      end

      it 'must set invitation_accepted_at to current date' do
        travel_to Time.zone.now do
          expect { patch_request }.to(
            change { manager.reload.invitation_accepted_at }
              .from(nil).to(Time.current),
          )
        end
      end

      it 'must redirect to root_url' do
        patch_request
        expect(response).to redirect_to(root_url)
      end

      it 'must authenticate manager' do
        patch_request
        expect(cookies.signed[:jwt].presence).to eq(manager.auth_token)
      end
    end

    describe 'with invalid params' do
      let(:params) do
        {
          token: manager.invitation_token,
          manager: {
            name: '',
            password: 'test',
            password_confirmation: 'test',
          }
        }
      end

      it 'must not remove invitation token' do
        expect { patch_request }
          .not_to change { manager.reload.invitation_token }
      end

      it 'must not set accepted_at date' do
        expect { patch_request }
          .not_to change { manager.reload.invitation_accepted_at }
      end

      it 'must not authenticate manager' do
        expect { patch_request }
          .not_to change { cookies.signed[:jwt] }
      end

      it 'must render new' do
        patch_request
        expect(response).to render_template(:new)
      end

      describe 'when name do not exist' do
        it 'must have error on name' do
          patch_request
          expect(assigns(:manager).errors[:name].count).to be(1)
        end

        it 'must have one no more errors on manager' do
          patch_request
          expect(assigns(:manager).errors.count).to be(1)
        end
      end

      describe 'when token do not exist' do
        let(:params) do
          {
            token: 'nil',
            manager: {
              name: '',
              password: 'test',
              password_confirmation: 'test',
            }
          }
        end

        it 'must redirect to root_url' do
          patch_request
          expect(response).to redirect_to(root_url)
        end
      end

      describe 'when password do not exist' do
        let(:params) do
          {
            token: manager.invitation_token,
            manager: {
              name: 'Toto',
              password_confirmation: 'test',
            },
          }
        end

        it 'must not have errors on base' do
          patch_request
          expect(assigns(:manager).errors[:base]).to be_empty
        end

        it 'must have error on password' do
          patch_request
          expect(assigns(:manager).errors[:password].count).to be(1)
        end
      end

      describe 'when password confirmation do not exist' do
        let(:params) do
          {
            token: manager.invitation_token,
            manager: {
              name: 'Toto',
              password: 'test',
            }
          }
        end

        it 'must not have errors on base' do
          patch_request
          expect(assigns(:manager).errors[:base]).to be_empty
        end

        it 'must not have error on password' do
          patch_request
          expect(assigns(:manager).errors[:password_confirmation].count)
            .to be(1)
        end

        it 'must have no more errors on manager' do
          patch_request
          expect(assigns(:manager).errors.count).to be(1)
        end
      end

      describe 'when password confirmation is not the same' do
        let(:params) do
          {
            token: manager.invitation_token,
            manager: {
              name: 'Toto',
              password: 'test',
              password_confirmation: 'tset',
            }
          }
        end

        it 'must not have errors on base' do
          patch_request
          expect(assigns(:manager).errors[:base]).to be_empty
        end

        it 'must not have error on password' do
          patch_request
          expect(assigns(:manager).errors[:password_confirmation].count)
            .to be(1)
        end

        it 'must have no more errors on manager' do
          patch_request
          expect(assigns(:manager).errors.count).to be(1)
        end
      end
    end
  end

end
