require 'rails_helper'

RSpec.describe ManagerSessionsController, type: :controller do

  describe "#new" do
    it "should render new template" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe "#create" do
    let(:password) { Faker::Alphanumeric.alphanumeric(number: 10) }
    let(:manager) { FactoryBot.create(:manager, password: password) }

    describe "with invalid parameters" do
      it "should render new if manager does not exist" do
        post :create,
             params: { email: 'wrong@example.net', password: password }
        expect(response).to render_template(:new)
      end

      it "should responds with 401 if password is invalid" do
        post :create,
             params: { email: manager.email, password: 'wrong' }
        expect(response).to render_template(:new)
      end

      it "should not send a jwt cookie" do
        post :create,
             params: { email: manager.email, password: 'wrong' }
        expect(cookies.signed[:jwt]).to be_nil
      end
    end

    describe "as a manager with valid parameters" do
      it "sould redirect to session url" do
        post :create,
             params: { email: manager.email, password: password }
        expect(response).to redirect_to(root_url)
      end

      it "should send a jwt cookie" do
        post :create,
             params: { email: manager.email, password: password }
        expect(cookies.signed[:jwt]).not_to be_nil
      end

      it "should set the expiracy of the cookie in 1.hour" do
        stub_cookie_jar = double
        allow(stub_cookie_jar).to receive(:signed)
          .and_return(HashWithIndifferentAccess.new)
        allow(controller).to receive(:cookies).and_return(stub_cookie_jar)

        post :create,
             params: { email: manager.email, password: password }
        expiring_cookie = stub_cookie_jar.signed[:jwt]
        expect(expiring_cookie[:expires].to_i)
          .to be_within(1).of(3.months.from_now.to_i)
      end

      it "should send the authenticity token as a jwt token" do
        post :create,
             params: { email: manager.email, password: password }
        expect(cookies.signed[:jwt]).to eq(manager.auth_token)
      end
    end
  end

  describe "#destroy" do
    let(:manager) { FactoryBot.create(:manager) }
    let(:stub_cookie_jar) { authenticate(manager.auth_token, controller) }

    before(:each) { allow(stub_cookie_jar).to receive(:delete).with(:jwt) }

    it "should require authentication" do
      expect(controller).to receive(:require_login)
        .and_call_original
      delete :destroy
    end

    it "should logout the manager" do
      expect(stub_cookie_jar).to receive(:delete).with(:jwt)
      delete :destroy
    end

    it "should redirect to session new" do
      delete :destroy
      expect(response).to redirect_to(root_url)
    end
  end

end
