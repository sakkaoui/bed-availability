require 'rails_helper'

RSpec.describe EntitiesController, type: :controller do

  describe "#avalabilities" do
    describe "with valid token" do
      let(:entity) { FactoryBot.create(:entity) }
      let(:get_request) { get :availabilities, params: { token: entity.token } }

      it "should render availabilities template" do
        get_request
        expect(response).to render_template(:availabilities)
      end

      it "should find corresponding entity" do
        get_request
        expect(assigns(:entity)).to eq(entity)
      end

      describe "with services" do
        let!(:services) { FactoryBot.create_list(:service, 5, entity: entity) }

        it "should list entity's services" do
          get_request
          expect(assigns(:services).to_set).to eq(services.to_set)
        end

      end
    end

    describe "with invalid token" do
      let(:get_request) { get :availabilities, params: { token: "fake-token" } }

      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe "#qrcode" do
    describe "with valid token" do
      let(:entity) { FactoryBot.create(:entity) }
      let(:get_request) { get :qrcode, params: { token: entity.token } }

      it "should render availabilities template" do
        get_request
        expect(response).to render_template(:qrcode)
      end

      it "should find corresponding entity" do
        get_request
        expect(assigns(:entity)).to eq(entity)
      end
    end

    describe "with invalid token" do
      let(:get_request) { get :qrcode, params: { token: "fake-token" } }

      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe '#new' do
    let(:get_request) { get :new }

    it 'must render new template' do
      get_request
      expect(response).to render_template(:new)
    end

    it 'must have a manager build' do
      get_request
      expect(assigns(:entity).managers.size).to be(1)
    end
  end

  describe('#create') do
    let(:params) do
      {
        entity: FactoryBot.attributes_for(:entity).merge({
          managers_attributes: {
            '0' => {
              name: Faker::Name.name,
              email: Faker::Internet.email,
              password: 'password',
              password_confirmation: 'password',
            },
          },
        }),
      }
    end

    let(:post_request) { post(:create, params: params) }

    describe 'with valid params' do
      it 'must create a new entity' do
        expect { post_request }.to change { Entity.count }.by(1)
      end

      it 'must create a new manager' do
        expect { post_request }.to change { Manager.count }.by(1)
      end

      it 'must not have errors on entity' do
        post_request
        expect(assigns(:entity).errors).to be_empty
      end
    end

    describe 'with invalid params' do
      let(:params) do
        {
          entity: FactoryBot.attributes_for(:entity).merge({
            managers_attributes: {
              '0' => {
                email: Faker::Internet.email,
                password: 'password',
                password_confirmation: 'password'
              },
            },
          }),
        }
      end

      it 'must not create an entity' do
        expect { post_request }
          .not_to change { Entity.count }
      end

      it 'must not create a manager' do
        expect { post_request }
          .not_to change { Manager.count }
      end

      it 'must render new' do
        post_request
        expect(response).to render_template(:new)
      end

      describe 'when name do not exist' do
        it 'must not have errors on base' do
          post_request
          expect(assigns(:entity).errors[:base]).to be_empty
        end

        it 'must have error on name' do
          post_request
          expect(assigns(:entity).errors[:'managers.name'].count).to be(1)
        end

        it 'must have no more errors on entity' do
          post_request
          expect(assigns(:entity).errors.count).to be(1)
        end
      end

      describe 'when email do not exist' do
        let(:params) do
          {
            entity: FactoryBot.attributes_for(:entity).merge({
              managers_attributes: {
                '0' => {
                  name: Faker::Internet.name,
                  password: 'password',
                  password_confirmation: 'password',
                },
              },
            }),
          }
        end

        it 'must not have errors on base' do
          post_request
          expect(assigns(:entity).errors[:base]).to be_empty
        end

        it 'must have error on email' do
          post_request
          expect(assigns(:entity).errors[:'managers.email'].count).to be(2)
        end

        it 'must have no more errors on entity' do
          post_request
          expect(assigns(:entity).errors.count).to be(2)
        end
      end

      describe 'when password confirmation do not exist' do
        let(:params) do
          {
            entity: FactoryBot.attributes_for(:entity).merge({
              managers_attributes: {
                '0' => {
                  name: Faker::Internet.name,
                  email: Faker::Internet.email,
                  password: 'password',
                },
              },
            }),
          }
        end

        it 'must not have errors on base' do
          post_request
          expect(assigns(:entity).errors[:base]).to be_empty
        end

        it 'must not have error on password' do
          post_request
          expect(
            assigns(:entity).errors[:'managers.password_confirmation'].count
          ).to be(1)
        end

        it 'must have no more errors on entity' do
          post_request
          expect(assigns(:entity).errors.count).to be(1)
        end
      end

      describe 'when password confirmation is not the same' do
        let(:params) do
          {
            entity: FactoryBot.attributes_for(:entity).merge({
              managers_attributes: {
                '0' => {
                  name: Faker::Internet.name,
                  email: Faker::Internet.email,
                  password: 'password',
                  password_confirmation: 'passowrd',
                },
              },
            }),
          }
        end

        it 'must not have errors on base' do
          post_request
          expect(assigns(:entity).errors[:base]).to be_empty
        end

        it 'must not have error on password' do
          post_request
          expect(
            assigns(:entity).errors[:'managers.password_confirmation'].count
          ).to be(1)
        end

        it 'must have no more errors on user' do
          post_request
          expect(assigns(:entity).errors.count).to be(1)
        end
      end
    end
  end

end
