require 'rails_helper'

RSpec.describe ServiceRefereesController, type: :controller do

  describe "#new" do
    let(:service) { FactoryBot.create(:service) }
    let(:get_request) { get :new, params: { service_id: service.id } }

    describe 'authenticated' do
      let(:manager) { FactoryBot.create(:manager, entity: service.entity) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        get_request
      end

      it 'must render new template' do
        get_request
        expect(response).to render_template(:new)
      end

      it "should assigns a service_referee" do
        get_request
        expect(assigns(:referee)).to be_a(ServiceReferee)
      end

      it "should assigns a service_referee for the current entity" do
        get_request
        expect(assigns(:referee).entity).to eq(manager.entity)
      end

      it "should assigns a service_referee for the current service" do
        get_request
        expect(assigns(:referee).services).to eq([service])
      end

      describe "with another service from another entity" do
        it "should redirect to home" do
          get :new, params: { service_id: FactoryBot.create(:service).id }
          expect(response).to redirect_to('/')
        end
      end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        get_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe "#create" do
    let(:service) { FactoryBot.create(:service) }
    let(:referee_attributes) do
      {
        email: Faker::Internet.email,
        name: Faker::Name.name,
        contact_details: Faker::Lorem.words.join(' ')
      }
    end
    let(:post_request) do
      post :create, params: {
        service_id: service.id,
        service_referee: referee_attributes
      }
    end

    describe 'authenticated' do
      let(:manager) { FactoryBot.create(:manager, entity: service.entity) }

      before(:each) { authenticate(manager.auth_token, controller) }

      it 'must require authentication' do
        expect(controller).to receive(:require_login)
          .and_call_original
        post_request
      end

      describe "with valid parameters" do
        it 'must render new template' do
          post_request
          expect(response).to redirect_to(services_url)
        end

        it "should create a service_referee" do
          expect do
            post_request
          end.to change { ServiceReferee.count }.by(1)
        end

        it "should create a service_referee for the targeted service" do
          expect do
            post_request
          end.to change { service.reload.service_referees.count }.by(1)
        end

        it "should create a service_referee for the targeted entity" do
          expect do
            post_request
          end.to change { manager.entity.reload.managers.count }.by(1)
        end

        it "should save given fields" do
          post_request
          referee = ServiceReferee.last
          expect(referee.email).to eq(referee_attributes[:email])
          expect(referee.name).to eq(referee_attributes[:name])
          expect(referee.contact_details)
            .to eq(referee_attributes[:contact_details])
        end

        describe "with already existing referee in this service" do
          let!(:referee) do
            FactoryBot.create(
              :assignation,
              service: service,
              service_referee: FactoryBot.create(
                :service_referee,
                email: referee_attributes[:email],
                entity: service.entity
              )
            )
          end

          it 'must render new template' do
            post_request
            expect(response).to redirect_to(services_url)
          end

          it "should not create a service_referee" do
            expect do
              post_request
            end.not_to change { ServiceReferee.count }
          end

          it "should not create a service_referee for the targeted service" do
            expect do
              post_request
            end.not_to change { service.reload.service_referees.count }
          end
        end

        describe "with already existing referee in another service" do
          let!(:referee) do
            FactoryBot.create(
              :assignation,
              service_referee: FactoryBot.create(
                :service_referee,
                email: referee_attributes[:email],
                entity: service.entity
              )
            )
          end

          it 'must render new template' do
            post_request
            expect(response).to redirect_to(services_url)
          end

          it "should not create a service_referee" do
            expect do
              post_request
            end.not_to change { ServiceReferee.count }
          end

          it "should create a service_referee for the targeted service" do
            expect do
              post_request
            end.to change { service.reload.service_referees.count }.by(1)
          end
        end

        describe "with another service from another entity" do
          it "should redirect to home" do
            post :create, params: {
              service_id: FactoryBot.create(:service).id,
              service_referee: referee_attributes
            }
            expect(response).to redirect_to('/')
          end
        end
      end

      describe "with invalid params" do
        let(:referee_attributes) do
          {
            email: Faker::Internet.email,
            name: nil,
            contact_details: Faker::Lorem.words.join(' ')
          }
        end

        it 'must render new' do
          post_request
          expect(response).to render_template(:new)
        end

        it "should not create a service_referee" do
          expect do
            post_request
          end.not_to change { ServiceReferee.count }
        end

        it "should not create a service_referee for the targeted service" do
          expect do
            post_request
          end.not_to change { service.reload.service_referees.count }
        end

        it "should not create a service_referee for the targeted entity" do
          expect do
            post_request
          end.not_to change { manager.entity.reload.managers.count }
        end
      end
    end

    describe "unauthenticated" do
      it "should redirect to home" do
        post_request
        expect(response).to redirect_to('/')
      end
    end
  end

  describe "#destroy" do
    let(:service) { FactoryBot.create(:service) }
    let(:manager) { FactoryBot.create(:manager, entity: service.entity) }

    describe 'authenticated' do

      before(:each) { authenticate(manager.auth_token, controller) }

      describe "with existing manager from same entity" do
        let!(:referee_to_be_destroyed) do
          Assignation.create(
            service_referee: FactoryBot.create(
              :service_referee, entity: manager.entity
            ),
            service: service
          )
        end

        describe "with other assignments" do
          let!(:another_assignment) do
            Assignation.create(
              service_referee: referee_to_be_destroyed.service_referee,
              service: FactoryBot.create(:service, entity: manager.entity)
            )
          end

          it "should destroy the assignment" do
            expect(
              Assignation.where(
                service_id: service.id,
                service_referee: referee_to_be_destroyed.service_referee_id
              )
            ).to exist
            delete :destroy, params: {
              service_id: service.id,
              id: referee_to_be_destroyed.service_referee_id
            }
            expect(
              Assignation.where(
                service_id: service.id,
                service_referee: referee_to_be_destroyed.service_referee_id
              )
            ).not_to exist
          end

          it "should not destroy the referee" do
            expect do
              delete :destroy, params: {
                service_id: service.id,
                id: referee_to_be_destroyed.service_referee_id
              }
            end.not_to change { ServiceReferee.count }
          end
        end

        describe "with no other assignments" do
          it "should destroy the assignment" do
            expect(
              Assignation.where(
                service_id: service.id,
                service_referee: referee_to_be_destroyed.service_referee_id
              )
            ).to exist
            delete :destroy, params: {
              service_id: service.id,
              id: referee_to_be_destroyed.service_referee_id
            }
            expect(
              Assignation.where(
                service_id: service.id,
                service_referee: referee_to_be_destroyed.service_referee_id
              )
            ).not_to exist
          end

          it "should destroy the referee" do
            expect do
              delete :destroy, params: {
                service_id: service.id,
                id: referee_to_be_destroyed.service_referee_id
              }
            end.to change { ServiceReferee.count }.by(-1)
          end
        end

        it "should redirect to referee list" do
          delete :destroy, params: {
            service_id: service.id, id: referee_to_be_destroyed.id
          }
          expect(response)
            .to redirect_to(new_service_service_referee_path(service))
        end
      end

      describe "with existing referee from another service" do
        let(:another_service) do
          FactoryBot.create(:service, entity: service.entity)
        end
        let!(:referee_to_be_destroyed) do
          Assignation.create(
            service_referee: FactoryBot.create(
              :service_referee, entity: manager.entity
            ),
            service: another_service
          )
        end

        it "should not destroy the referee" do
          expect(
            Assignation.where(
              service_id: another_service.id,
              service_referee: referee_to_be_destroyed.service_referee_id
            )
          ).to exist
          delete :destroy, params: {
            service_id: service.id,
            id: referee_to_be_destroyed.service_referee_id
          }
          expect(
            Assignation.where(
              service_id: another_service.id,
              service_referee: referee_to_be_destroyed.service_referee_id
            )
          ).to existing
        end

        it "should redirect to referee list" do
          delete :destroy, params: {
            service_id: service.id, id: referee_to_be_destroyed.id
          }
          expect(response)
            .to redirect_to(new_service_service_referee_path(service))
        end
      end

      describe "with bad id" do
        it "should not destroy any assignment" do
          expect do
            delete :destroy, params: { service_id: service.id, id: "fake-id" }
          end.not_to change { Assignation.count }
        end

        it "should redirect to referee list" do
          delete :destroy, params: { service_id: service.id, id: "fake-id" }
          expect(response)
            .to redirect_to(new_service_service_referee_path(service))
        end
      end
    end

    describe "unauthenticated" do
      it "should redirect to /" do
        delete :destroy, params: { service_id: service.id, id: "fake-id" }
        expect(response).to redirect_to(root_url)
      end
    end
  end

end
