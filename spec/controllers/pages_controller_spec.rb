require 'rails_helper'

RSpec.describe PagesController, type: :controller do

  describe "#home" do
    let(:get_request) { get :home }

    it "should render home template" do
      get_request
      expect(response).to render_template(:home)
    end
  end
end
