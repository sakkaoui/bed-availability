require 'rails_helper'

RSpec.describe 'Manager sessions routes', type: :routing do

  it 'must route to manager_sessions#new' do
    expect(get: '/managers/sessions/new')
      .to route_to(controller: 'manager_sessions', action: 'new')
  end

  it 'must route to manager_sessions#create' do
    expect(post: '/managers/sessions')
      .to route_to(controller: 'manager_sessions', action: 'create')
  end

end
