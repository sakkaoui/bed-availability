require 'rails_helper'

RSpec.describe 'Entities routes', type: :routing do

  it 'must route to entities#new' do
    expect(get: '/entities/new')
      .to route_to(controller: 'entities', action: 'new')
  end

  it 'must route to entities#create' do
    expect(post: '/entities')
      .to route_to(controller: 'entities', action: 'create')
  end

end
