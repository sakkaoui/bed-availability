require 'rails_helper'

RSpec.describe 'Entities routes', type: :routing do

  it 'must route to assignations#disable_notifications' do
    expect(delete: '/services/1/assignations/disable_notifications')
      .to route_to(
        controller: 'assignations',
        action: 'disable_notifications',
        service_id: "1"
      )
  end

  it 'must route to assignations#enable_notifications' do
    expect(post: '/services/1/assignations/enable_notifications')
      .to route_to(
        controller: 'assignations',
        action: 'enable_notifications',
        service_id: "1"
      )
  end

end
